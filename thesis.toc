\contentsline {chapter}{Introduction}{1}{chapter*.4}%
\contentsline {chapter}{\numberline {1}Wave optics}{5}{chapter.1}%
\contentsline {section}{\numberline {1.1}From Maxwell's equations to Fresnel diffraction}{6}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}Maxwell's equations in free space}{6}{subsection.1.1.1}%
\contentsline {subsection}{\numberline {1.1.2}The wave equation}{6}{subsection.1.1.2}%
\contentsline {subsection}{\numberline {1.1.3}The Helmholtz equation}{7}{subsection.1.1.3}%
\contentsline {subsection}{\numberline {1.1.4}The paraxial Helmholtz equation}{8}{subsection.1.1.4}%
\contentsline {section}{\numberline {1.2}Optical waveguides}{9}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}The paraxial Helmholtz equation for a graded-index waveguide}{9}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Parabolic waveguide in one dimension}{11}{subsection.1.2.2}%
\contentsline {chapter}{\numberline {2}Neural networks}{13}{chapter.2}%
\contentsline {section}{\numberline {2.1}What is a neural network?}{13}{section.2.1}%
\contentsline {section}{\numberline {2.2}Mathematics of feedforward neural networks}{15}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Vocabulary and notation}{15}{subsection.2.2.1}%
\contentsline {paragraph}{Types of data}{15}{paragraph*.6}%
\contentsline {paragraph}{Input, output and hidden layers}{15}{paragraph*.7}%
\contentsline {paragraph}{Weights and biases}{16}{paragraph*.8}%
\contentsline {paragraph}{Hyperparameters}{16}{paragraph*.9}%
\contentsline {subsection}{\numberline {2.2.2}Forward propagation}{16}{subsection.2.2.2}%
\contentsline {paragraph}{Activation function}{17}{paragraph*.10}%
\contentsline {subsection}{\numberline {2.2.3}Backpropagation}{18}{subsection.2.2.3}%
\contentsline {paragraph}{Loss and cost function}{18}{paragraph*.11}%
\contentsline {paragraph}{Minimizing the cost function}{18}{paragraph*.12}%
\contentsline {paragraph}{Gradient descent}{19}{paragraph*.13}%
\contentsline {paragraph}{Derivative of the cost function}{20}{paragraph*.14}%
\contentsline {subsection}{\numberline {2.2.4}Stochastic gradient descent}{21}{subsection.2.2.4}%
\contentsline {paragraph}{First-order methods}{22}{paragraph*.15}%
\contentsline {paragraph}{Second-order methods}{22}{paragraph*.16}%
\contentsline {subsection}{\numberline {2.2.5}Overfitting}{23}{subsection.2.2.5}%
\contentsline {section}{\numberline {2.3}Implementation of a deep neural network as a function approximator}{24}{section.2.3}%
\contentsline {paragraph}{Defining the problem}{25}{paragraph*.18}%
\contentsline {paragraph}{Implementation}{25}{paragraph*.19}%
\contentsline {paragraph}{Results}{27}{paragraph*.20}%
\contentsline {chapter}{\numberline {3}Physics-informed neural networks}{29}{chapter.3}%
\contentsline {section}{\numberline {3.1}Idea and mathematics of PINNs}{29}{section.3.1}%
\contentsline {paragraph}{Notation}{30}{paragraph*.24}%
\contentsline {paragraph}{Cost function}{31}{paragraph*.25}%
\contentsline {section}{\numberline {3.2}Implementation of a simple PINN}{32}{section.3.2}%
\contentsline {paragraph}{Results}{33}{paragraph*.26}%
\contentsline {section}{\numberline {3.3}Modern developments}{36}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}Hyperparameters}{36}{subsection.3.3.1}%
\contentsline {subsection}{\numberline {3.3.2}Convergence properties}{36}{subsection.3.3.2}%
\contentsline {paragraph}{Adaptively choosing weights of loss components}{36}{paragraph*.30}%
\contentsline {paragraph}{Transfer learning}{37}{paragraph*.32}%
\contentsline {paragraph}{Where PINNs fail to train}{38}{paragraph*.33}%
\contentsline {paragraph}{Sequence-to-sequence learning}{38}{paragraph*.34}%
\contentsline {section}{\numberline {3.4}Summary}{39}{section.3.4}%
\contentsline {chapter}{\numberline {4}Solving the paraxial Helmholtz equation}{41}{chapter.4}%
\contentsline {section}{\numberline {4.1}Setting up the two-dimensional problem}{41}{section.4.1}%
\contentsline {paragraph}{Dimensionality reduction of the paraxial Helmholtz equation}{42}{paragraph*.36}%
\contentsline {paragraph}{Real and imaginary components}{42}{paragraph*.37}%
\contentsline {paragraph}{Boundary conditions}{42}{paragraph*.38}%
\contentsline {paragraph}{Normalization}{43}{paragraph*.39}%
\contentsline {paragraph}{Hyperparameters and other settings}{45}{paragraph*.41}%
\contentsline {section}{\numberline {4.2}Training results}{45}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Dirichlet boundary conditions}{45}{subsection.4.2.1}%
\contentsline {paragraph}{First normalization}{45}{paragraph*.42}%
\contentsline {paragraph}{Beam width}{47}{paragraph*.45}%
\contentsline {paragraph}{Notes on the training process}{48}{paragraph*.47}%
\contentsline {paragraph}{Reflections at the boundary}{49}{paragraph*.49}%
\contentsline {subsection}{\numberline {4.2.2}Periodic boundary conditions}{50}{subsection.4.2.2}%
\contentsline {paragraph}{Interference at the boundaries}{51}{paragraph*.53}%
\contentsline {subsection}{\numberline {4.2.3}Oddities: no boundary condition required}{51}{subsection.4.2.3}%
\contentsline {paragraph}{Why does the PINN converge?}{52}{paragraph*.55}%
\contentsline {paragraph}{Impact of number of collocation points}{53}{paragraph*.56}%
\contentsline {paragraph}{Extrapolation capabilities}{54}{paragraph*.59}%
\contentsline {section}{\numberline {4.3}Summary and discussion}{55}{section.4.3}%
\contentsline {chapter}{\numberline {5}Solving the parabolic waveguide}{57}{chapter.5}%
\contentsline {section}{\numberline {5.1}Preliminaries -- Setting up the problem}{57}{section.5.1}%
\contentsline {paragraph}{Real and imaginary components}{57}{paragraph*.60}%
\contentsline {paragraph}{Boundary condition}{58}{paragraph*.61}%
\contentsline {paragraph}{Initial conditions}{58}{paragraph*.62}%
\contentsline {paragraph}{Normalization}{60}{paragraph*.65}%
\contentsline {paragraph}{Hyperparameters and other settings}{61}{paragraph*.66}%
\contentsline {section}{\numberline {5.2}Training results}{61}{section.5.2}%
\contentsline {subsection}{\numberline {5.2.1}Guiding a beam with Gaussian profile}{61}{subsection.5.2.1}%
\contentsline {subsection}{\numberline {5.2.2}Guiding a superposition of two modes, $n=0$ and $n=1$}{63}{subsection.5.2.2}%
\contentsline {paragraph}{The training process}{64}{paragraph*.70}%
\contentsline {subsection}{\numberline {5.2.3}Guiding a superposition of four modes, $n=1$ to $n=4$}{65}{subsection.5.2.3}%
\contentsline {paragraph}{On the error of PINNs}{65}{paragraph*.72}%
\contentsline {section}{\numberline {5.3}Summary and discussion}{67}{section.5.3}%
\contentsline {chapter}{Conclusion and outlook}{69}{chapter*.75}%
\contentsline {chapter}{Appendix}{73}{chapter*.76}%
\contentsline {chapter}{References}{75}{equation.5.3.27}%
\contentsline {chapter}{Acknowledgments}{81}{chapter*.77}%
