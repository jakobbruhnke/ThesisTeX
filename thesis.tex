\documentclass[11pt,twoside]{book} % twoside for both-sided print. could be unnecessary.

%%%%%%%%%%%%%%%%% PACKAGES %%%%%%%%%%%%%%%%%

%% Layout %%

\usepackage[paper=a4paper,inner=35mm,
outer=25mm,top=25mm,bottom=25mm]{geometry} 	% standard for thesis in Germany
\usepackage[onehalfspacing]{setspace}		% change the vertical line space easily (onehalfspacing / singlespacing)
											% (onehalfspacing should be fine)
\usepackage{sectsty}                        % style section headings (e.g. to make them sans-serif)
\usepackage{fancyhdr}                       % what the name implies: fancy headers!
\usepackage{float}                          % für {figure}[H]
\usepackage[numbers]{natbib}				% also possible as options: square, sort&compress
\usepackage{titlesec}						% format chapter heading

%% Environments %%

\usepackage{array} 				% more options tabular-Env. (fixed cell dimensions)
\usepackage{graphicx}			% graphics inclusion (figure-env.)
\usepackage{wrapfig}			% for text and images parallel (rather finicky)
\usepackage[format=hang,
labelfont=bf,center,
position=top]{caption}			% for great captions (figures, tables etc.)
								% options can be modified; I like these options
\usepackage[labelformat=simple]{subcaption}			% enables subfigures and subtables with subcaptions;
								% subcaption package is dependend on caption package
%\usepackage{pdfpages}			% for including pdf files (problem with xcolor???)				
\usepackage{multicol}			% for multicolumns in tables
\usepackage{listings}			% for code inclusion
\usepackage{tabularx} 			% for line breaks in tables

%% Design, Fonts, Characters %%

\usepackage[T1]{fontenc}		% encoding output
\usepackage[utf8]{inputenc}		% LaTeX only offers ASCII support - but UTF-8 is awesome
\usepackage[table,dvipsnames]{xcolor}	% great colors (dvipsnames has further predefined)
\usepackage{hyperref}			% for hyperlinks (\hyperref, \url{})
\usepackage{pgfplots}			% has tikz included! used to create simple graphs
\usepackage[most]{tcolorbox}	% colored text boxes

%% Maths and Physics %%

\usepackage{siunitx}			% formatted units (\si{} for unit
					% \SI{}{} for number and unit
\usepackage{amsmath}			% basic math commands and env.
%\usepackage{amsfonts}			% basic math fonts
\usepackage{amsthm}				% nice theorem handling (for text boxes)
\usepackage{amssymb}			% basic math
\usepackage{mathtools}			% improves formatting of amsmath-stuff
\usepackage{bbm}				% because one needs to have \bbm{1} available
\usepackage{bm}					% slightly more comprehensive bold fonts
								% than covered by amsmath (bold greek letters)
\usepackage{physics}			% self-evidently for physics stuff
\usepackage{braket} 			% for \braket-command (handy in QM)
%\usepackage{icomma}             % comma formatting (for german text only!)
\usepackage{neuralnetwork}		% tikz neural networks

%% Fancy Equation Hyperreference Stuff

\usepackage{letltxmacro}		% for defining macros
\LetLtxMacro{\originaleqref}{\eqref} 	% save command that is due to be modified
\renewcommand{\eqref}{Eq.~\originaleqref}	% redefine


%%%%%%%%%%%%%%%%% PRE-DOCUMENT COMMANDS %%%%%%%%%%%%%%%%%

%% Layout-related stuff %%

\allsectionsfont{\sffamily} 	% make section titles sans-serif
%\setlength\parindent{0pt}		% no paragraph indent (matter of taste)
%\setlength{\parskip}{0pt}		% no paragraph skip
\raggedbottom					% prevents unimaginably ugly page stretching
\pagestyle{fancy}               % from fancyhdr
\fancyhf{}                      % clears the header and footer, otherwise the elements of the default "plain" page style will appear
\fancyhead[LE,RO]{\scshape\nouppercase{\leftmark}}  % print section title in header in smallcaps, left on even pages (LE) and right on odd pages (RO)
%\fancyhead[LO,RE]{\textsc{foo bar}} % print title in header in smallcaps, right on even pages (RE) and left on odd pages (LO)
\fancyfoot[LE,RO]{Page \thepage} % print page number in foot, left on even and right on odd pages

\fancypagestyle{plain}{% % <-- this is new
  \fancyhf{} 
  \fancyfoot[LE,RO]{Page \thepage} % same placement as with page style "fancy"
  \renewcommand{\headrulewidth}{0pt}}

%\newcolumntype{h}{>{\hsize=.6\hsize}X}

\hypersetup{colorlinks=true,allcolors=Blue}

\titleformat{\chapter}
  {\normalfont\sffamily\Huge\bfseries}{\thechapter}{1em}{}
\titlespacing*{\chapter}{0pt}{5em}{2em}


%% Colours %%    
\definecolor{salmon}{HTML}{fa8072}			% these are colors that I use in
\definecolor{yellowgreen}{HTML}{9acd32}		% my matplotlib-plots regularly
\definecolor{steelblue}{HTML}{4682b4}		% and maybe want to reference in TeX
\definecolor{whiteblue}{HTML}{96b4ce}
\definecolor{nodeblue}{HTML}{9999ff}	
\definecolor{nodelightblue}{HTML}{b2b2fa}
\definecolor{nodepurple}{HTML}{cd99ff}
\definecolor{nodegreen}{HTML}{99dfff}
\definecolor{nodecyan}{HTML}{c0d8e3}
\definecolor{tableblue}{rgb}{0.925,0.925,1} 
\definecolor{darktableblue}{rgb}{0.2,0.2,0.6} 
\definecolor{darktablegreen}{rgb}{0.2,0.5,0.2} 
\definecolor{darktablered}{rgb}{0.6,0.15,0.15} 

%% SI-related stuff %%
\sisetup{inter-unit-product=\cdot} 		% makes units separated by a multiplication dot
\sisetup{output-decimal-marker = {.}}	% makes decimal marker a dot (better in German: comma)
\sisetup{separate-uncertainty} 		% seperates unit from (value +- uncertainty)
\DeclareSIUnit\au{a.u.}				% defines atomic units
\DeclareSIUnit\kcal{kcal}			% defines kilo calories
\DeclareSIUnit\hz{Hz}				% defines Hertz
\DeclareSIUnit\fs{fs}				% defines femtosecond
\DeclareSIUnit\mm{mm}				% defines millimeter
\DeclareSIUnit\nm{nm}				% defines nanometer
\DeclareSIUnit\as{as}				% defines attosecond
\DeclareSIUnit\eV{eV}				% defines electron volt
\DeclareSIUnit\hart{Hartree}		% defines Hartree

%% Tikz and PGF-plots %%

\pgfplotsset{width=5in, height=3in,
	     compat=1.9}			% sets PGF plot dimensions and makes all plots 
	     						% compatible with version 1.9 and further
\usepgfplotslibrary{external}	% tiks and PGF plot are pathetically slow. these options
%\tikzexternalize				% ensure that each plot is compiled but once
								% and then stored and accessed from externally
\usetikzlibrary{calc}

%% Environments %%

\renewcommand{\arraystretch}{1.2}				% makes tables and arrays less cramped
\renewcommand{\figurename}{\textbf{Figure}}	% caption name for figures
\renewcommand{\tablename}{\textbf{Table}}		% caption name for tables
%\captionsetup[subfigure]{skip=-10pt}			% better subfigure-caption position
%\setlength\abovecaptionskip{50pt}
\setlength\belowcaptionskip{8pt}
			% better figure-caption position
\renewcommand\thesubfigure{\alph{subfigure})}	% make subfigure captions increase
												% alphabetically
\renewcommand\thesubtable{\alph{subtable})}	% make subcaption captions increase
												% alphabetically

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{tableblue},   
    commentstyle=\color{grey},
    keywordstyle=\color{darktableblue},
    stringstyle=\color{darktablegreen},
    %numberstyle=\color{darktablegreen},
    basicstyle=\ttfamily\small,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=4
}

\lstset{style=mystyle}

%%%%%%%%%%%%%%%%% NEW DEFINITIONS %%%%%%%%%%%%%%%%%

% Definitions that I've gotten used to. 
% These are all just shortcuts
% for existing definitions

\renewcommand{\i}{\mathbbm{i}}
\renewcommand{\d}{\mathrm{d}}
\newcommand{\pa}{\partial}
\newcommand{\act}{\mathrm{act}}
\newcommand{\hamil}{\hat{H}}
\newcommand{\efield}{\mathcal{E}}
\newcommand{\bfield}{\mathcal{B}}
\newcommand{\afield}{\mathcal{A}}
\newcommand{\hfield}{\mathcal{H}}
\newcommand{\pfield}{\mathcal{P}}
\newcommand{\dfield}{\mathcal{D}}
\newcommand{\mfield}{\mathcal{M}}
\newcommand{\efieldb}{\bm{\mathcal{E}}}
\newcommand{\bfieldb}{\bm{\mathcal{B}}}
\newcommand{\afieldb}{\bm{\mathcal{A}}}
\newcommand{\hfieldb}{\bm{\mathcal{H}}}
\newcommand{\pfieldb}{\bm{\mathcal{P}}}
\newcommand{\dfieldb}{\bm{\mathcal{D}}}
\newcommand{\mfieldb}{\bm{\mathcal{M}}}
\newcommand{\e}{\mathrm{e}}
\newcommand{\eps}{\varepsilon}
\newcommand{\A}{\mathbf{A}}
\newcommand{\B}{\mathbf{B}}
\newcommand{\C}{\mathbf{C}}
\renewcommand{\c}{\mathbf{c}}
\newcommand{\D}{\mathbf{D}}
\newcommand{\E}{\mathbf{E}}
\renewcommand{\H}{\mathbf{H}}
\newcommand{\F}{\mathbf{F}}
\renewcommand{\P}{\mathbf{P}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\MSE}{\mathrm{MSE}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\K}{\mathbb{K}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\T}{\mathrm{T}}
\newcommand{\rar}{\rightarrow}
\newcommand{\vphi}{\varphi}
\newcommand{\vthe}{\vartheta}
\newcommand{\vtheta}{\vartheta}
\newcommand{\bfnab}{\vec{\nabla}}
\newcommand{\br}{\mathbf{r}}
\newcommand{\bp}{\mathbf{p}}
\newcommand{\bk}{\mathbf{k}}
\newcommand{\bx}{\mathbf{x}}
\newcommand{\by}{\mathbf{y}}
\newcommand{\ppt}{\frac{\partial }{\partial t}}
\newcommand{\pptz}{\frac{\partial^2 }{\partial t^2}}
\newcommand{\ppx}{\frac{\partial }{\partial x}}
\newcommand{\ppxi}{\frac{\partial }{\partial \xi}}
\newcommand{\ppy}{\frac{\partial }{\partial y}}
\newcommand{\ppz}{\frac{\partial }{\partial z}}
\newcommand{\ppze}{\frac{\partial }{\partial \zeta}}
\newcommand{\ppzes}{\frac{\partial }{\partial \zeta'}}
\newcommand{\ppxz}{\frac{\partial^2 }{\partial x^2}}
\newcommand{\ppxiz}{\frac{\partial^2 }{\partial \xi^2}}
\newcommand{\ppxisz}{\frac{\partial^2 }{\partial \xi'^2}}
\newcommand{\ppyz}{\frac{\partial^2 }{\partial y^2}}
\newcommand{\ppzz}{\frac{\partial^2 }{\partial z^2}}
\newcommand{\ddt}{\frac{\mathrm{d}}{\mathrm{d}t}}
\newcommand{\ddx}{\frac{\mathrm{d}}{\mathrm{d}x}}
\newcommand{\ddxz}{\frac{\mathrm{d}^2}{\mathrm{d}x^2}}
\newcommand{\mrm}{\mathrm}
\newcommand{\inti}{\int_{-\infty}^\infty}

\newcommand\circledmark[1][cyan]{%
  \ooalign{%
    \hidewidth
    \kern0.65ex\raisebox{-0.9ex}{\scalebox{3}{\textcolor{#1}{\textbullet}}}
    \hidewidth\cr
    $\checkmark$\cr
  }%
}

\newcommand\circledomark[1][orange]{%
  \ooalign{%
    \hidewidth
    \kern0.65ex\raisebox{-0.9ex}{\scalebox{3}{\textcolor{#1}{\textbullet}}}
    \hidewidth\cr
    $\checkmark$\cr
  }%
}

\newcommand\circledcross[1][red]{%
  \ooalign{%
    \hidewidth
    \kern0.5ex\raisebox{-1.15ex}{\scalebox{3}{\textcolor{#1}{\textbullet}}}
    \hidewidth\cr
    $\times$\cr
  }%
}


\newcommand*{\SignatureAndDate}[1]{%
    \par\makebox[4cm]{Jena,} \makebox[3cm]{\hrulefill}  \hfill\makebox[4cm]{\hrulefill}%
    \par\makebox[4cm]{} \makebox[3cm]{\centering Date}     \hfill\makebox[4cm]{\centering Signature}%
}


\newcounter{box}
\renewcommand\thebox{\thechapter.\arabic{box}} 
\tcbmaketheorem{deeperlook}{A deeper look}{
breakable,
enhanced,
fonttitle=\bfseries\sffamily,
colback=tableblue,
colframe=darktableblue
}{box}{bx}

%%%%%%%%%%%%%%%%% DOCUMENT %%%%%%%%%%%%%%%%%


\begin{document}


%%%% DECKBLATT %%%%

\thispagestyle{empty}

\begin{figure}[H]
\flushleft
\includegraphics[width=.7\textwidth]{logo.jpg}
\end{figure}

\vspace*{20pt}

\begin{flushright}
{\LARGE \textsc{Physikalisch-Astronomisches Institut}\\}
\vspace*{5pt}
{\Large \textsc{Institute of Applied Physics}}
\end{flushright}

\vspace*{50pt}

\begin{minipage}[.32\textheight]{0.15\textwidth}
\begin{flushright}
	\rule{1pt}{.32\textheight}
\end{flushright}
\end{minipage}
\begin{minipage}[.32\textheight]{.05\textwidth}
\hspace{1pt}
\end{minipage}
\begin{minipage}[.32\textheight]{0.80\textwidth}
\begin{flushleft}
 % Vertical line
	{\LARGE \textsc{\textbf{Predicting propagation of paraxial waves using physics-informed neural networks}}}

	\vspace{30pt}

	{\Large \textsc{Bachelor Thesis}}
	
	\vspace{30pt}
	
	{\large \textsc{Jakob Bruhnke}}
	
	\vspace{20pt}
	
	{\textsc{Born on $4^\text{th}$ November $1999$}\\}
	{\textsc{in Magdeburg}}
	
\end{flushleft}
\end{minipage}

%%%% BLANK PAGE

\newpage\null\thispagestyle{empty}\newpage

%%%% PRÜFER

\newpage

\thispagestyle{empty}

{\large
\begin{table}[H]
\centering
\begin{tabular}{ll}
First Referee: & Prof.~Dr.~Stefan~Nolte \\
Second Referee: & Dr.~Jisha~Chandroth~Pannian \\
Supervisors : & Dr.~Jisha~Chandroth~Pannian \\
& \& Dr.~Alessandro~Alberucci
\end{tabular}
\end{table}
}

\vspace{50pt}

\begin{center}
Submitted on $28^\text{th}$ July 2022
\end{center}


\vfill

\section*{Declaration of academic integrity and consent to publishing}

I hereby declare that I wrote the present thesis titled

\begin{center} \noindent\textsc{Predicting propagation of paraxial waves \\ \noindent{}using physics-informed neural networks} \end{center}

\noindent independently on my own and without any other resources than the ones indicated. All thoughts taken directly or indirectly from external sources are properly denoted as such. This thesis has neither been previously submitted to another authority, nor has it been published yet. \\ 

\noindent The author consents to this thesis being made available to the general public by the \textit{Thüringer Universitäts- und Landesbibliothek}.



\vspace*{30pt}

\SignatureAndDate{}

\vspace*{30pt}

%%%% BLANK PAGE

\newpage\null\thispagestyle{empty}\newpage

%%%% TABLE OF CONTENTS

\newpage

\thispagestyle{empty}

\tableofcontents

\thispagestyle{empty}

%%%% INTRODUCTION

\newpage

\clearpage
\setcounter{page}{1}


\input{chapters/introduction}

\begin{comment}
\paragraph{things to remember}
\begin{itemize}
\item provide new vocabulary in \textit{italic} font (don't repeat; check when thesis is finished)
\item put : before equations, not a dot
\item things can't have a possessive apostrophe (``the beam's property'' is wrong, ``the property of the beam'' is correct)
\end{itemize}





\begin{itemize}
\item PDEs govern all our physical world
\item most PDEs don't have closed-form solution
\item two solutions: either approximate until closed-form solution exists (pertubation theory) or use computational methods.
\item not without reason: computational physics third column of physics next to theoretical and experimental physics -- very powerful, many many experimental papers today are accompanied and in part verified by computational methods
\item toolbox of solving partial differential equations is very big: most popular are finite difference, finite element, finite volume and spectral element methods. very established, work very well for many tested problems, established code bases with shallow learning curves
\item problems: 
\begin{enumerate}
\item computational cost. ``curse of dimensionality'' (is actually called differently). 3D problems often require extraordinarily many mesh points. many partial differential equations are much more higher-dimensional, e.g. found in mathematical finance or quantum physics
\item discretization errors -- grid size must be able to accomodate for high rates of changes of the solution. for time-stepping schemes, the time steps sometimes must be extraordinarily small
\end{enumerate} 
\item big thematic switch: neural networks
\item NNs have transformed our world in astonishing speed: computer vision, natural language processing, classification, pattern recognition, regression problems
\item within the last decade, it has also ``invaded'' the natural science research with prediction of protein folding, prediction of quantum chemistry DFT stuff, drug discovery and medical imaging. over the last five years, a new class of deep learning networks, physics-informed neural networks (PINNs) have emerged as method for solving partial differential equations
\item at first: one may be sceptical concerning use of NNs to solve PDEs -- for usual NNs, huge amounts of data are necessary for training and for many of these PDE problems, we are firmly in the sparse data regime
\item however, what such PDE problems all have in common is that we have prior information in form of the PDE
\item PINNs incorporate this prior information of the PDE together with initial and boundary conditions into the NN by penalizing predictions which do not fulfil the PDE or init/bound conditions. in fact, while prior data can definetely help at converence, it is not required. PINNs can learn unsupervised without labelled data from experiments or other simulations. PINNs are gridless - any point can be taken as input; no mesh required.
\item essential method for this: automatic differentiation! has only within the last decade been fusioned with NN research. autodiff is a class of techniques which allow for calculation of numerical derivatives -- however, not a discrete method and thus doesn't suffer from discretization error. also not a symbolic method and thus doesn't suffer from ``expression swell'' and poor speed. allows to calculate derivatives of the model with respect to arbitrary parameters $\rar$ derivatives of PDEs can be calculated.
\item PINNs are at the early research stages. they have produced promising results (papeeeers) but have also some undesirable properties -- convergence is unreliable, errors may be rather big, performance for ``small scale problems'' rather bad. for all these properties, there are already many research works trying to mitigate them. the headstart of discrete methods is palpable. only time will tell if PINNs will establish themselves as solvers of PDEs.
\item implementation of PINNs is rather simple and can be done in machine learning libraries such as tensorflow or torch. implementation in these frameworks is very flexible and tensorflow will indeed be used in this thesis. however, basic knowledge of neural networks is basically required for using these libraries. obviously, if PINNs are to become established methods used throughout the natural science community, we need PINN libraries which serve as ``black boxes''; no prior knowledge of NNs required. in fact, these libraries already exist: DeepDXE and \textbf{some nvidia stuff} are two examples. in particular, DeepDXE has been used manifold in research.
\item \textbf{Perspektivisch:} PINNs will be used for problems where no reference solution is available. If we are to reach this point however, simple \textit{toy problems} are \textbf{unerlässlich} in order to gain knowledge on how PINNs train, under which conditions they struggle to converge and which parameters or methods have the highest impact on performance. 
\item Besides \textbf{blablabla (geology, hydrology)}, one dominant application of discrete numerical methods is the description of light propagation -- optics. The aim of this thesis is to investigate the application of PINNs to two optical \textit{toy problems}: propagation of a Gaussian beam and propagation of light in a parabolic wave guide.
\item new topic: OPTICS
\item optics is one of the most important fields in physics. the impact on our lives today cannot be overstated -- \textbf{examples}. there are three dominant theoretical frameworks for the description of light: ray optics, where light is modelled via light rays, wave optics where light is modelled via scalar waves adhering to the wave equation and finally vector optics, where light is described as electromagnetic vector fields solving Maxwell's equation. 
\item A subfield of scalar optics is beam optics -- the optical description of scalar waves which vary slowly in the direction of optical propagation, also called \textit{paraxial waves}. beam optics has proven paramount in the description of laser light. there, the field strength is described via a complex amplitude given by the \textit{paraxial Helmholtz equation}. Perhaps the most well-known solution of the paraxial Helmholtz equation is the Gaussian beam; being a model for perfect laser light.
\item In today's world, it has become paramount to be able to transport laser light over long distances. This is achieved via waveguides; optical constructs which contain light by internal reflections by embedding a light conduit in some medium of higher refractive index. This can be done both with step-index media, where the refractive index changes abruptly at the interface of two different materials, or with graded-index media, where the refractive index increasing gradually in the direction transverse to the optical axis.
\item ``Realistic'' graded-index waveguides (examples!) are complicated to describe in the framework of scalar or beam optics. As toy problem to study with a PINN, we choose a parabolic waveguide, i.e. a medium whose refractive index increases quadratically transversely to the optical axis since for propagation of paraxial waves in this waveguide, a closed-form analytical solution exists in the form of Hermite-Gaussian beams.
\item new topic: THESIS STRUCTURE
\item first: theory of optical propagation of paraxial beams both in free space and in a parabolic wave guide
\item then: basic theory of neural networks; NN as universal function approximator will be demonstrated by approximating a sine function
\item then: theory behind PINNs; simple PINN for 1D poisson equation presented; since PINNs are rapidly developing field of research much \textit{cutting-edge} progress will not be incorporated. however, a small review on modern developments will be presented.
\item gaussian beam propagation: impact of boundary condition! intuition of training process! can the PINN provide reasonable prediction outside the training region?
\item waveguide propagation: impact of initial condition! 
\end{itemize}
\end{comment}





%%%%%%%%%%%%%%%%%%%%



\input{chapters/c1_optics}

\input{chapters/c2_nns}

\input{chapters/c3_pinns}

\input{chapters/c4_gaussian_restructured}

\input{chapters/c5_waveguide}

%\input{chapters/c4_optics_fw}

%\input{chapters/c5_optics_inv}

\input{chapters/summary}






\input{chapters/supplementary}

\newpage
\addcontentsline{toc}{chapter}{References}

\bibliographystyle{naturemag}
\bibliography{PINNs}



\newpage
\addcontentsline{toc}{chapter}{Acknowledgments}

\chapter*{Acknowledgments}

First, I want to express my sincere gratitude to Prof. Stefan Nolte for his supervision, many helpful comments concerning the manuscript and for entrusting me with this exciting topic.\\
\\
This work would not have been possible without the constant guidance and help from Dr. Jisha Chandroth Pannian (my second referee) and Dr. Alessandro Alberucci. Both helped me with interpretation of results, offered suggestions on which paths to explore further in simulations, and, with their expertise in optics, gave me much needed context for my calculations. Alessandro in particular did extensive proof-reading and thus improved, without exaggeration, every page of this thesis. For both of their great effort, help and advice, I am greatly indebted to them.\\
\\
Finally, I want to thank my girlfriend, Anna, for proof-reading parts of this thesis.




\end{document}

