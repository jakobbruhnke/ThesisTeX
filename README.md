# ThesisTeX

Repo of my bachelor thesis on PINNs.

## Installation
TeXLive should do the trick.

## Roadmap

- [x] create repository

- [ ] [invite](https://docs.gitlab.com/ee/user/project/members/) supervisors


- [ ] Finish chapter on Gaussian optics
  - [ ] collect first ideas
  - [ ] outline


- [ ] Finish chapter on neural networks
  - [x] collect first ideas
  - [ ] outline


- [ ] Finish chapter on PINNs
  - [ ] collect first ideas
  - [ ] outline


## Authors and acknowledgment
**Author**: Jakob Bruhnke

**Supervisors**: Dr. Jisha Chandroth Pannian & Dr. Alessandro Alberucci

**Referees**: Prof. Dr. Stefan Nolte & Dr. Jisha Chandroth Pannian
