\chapter[Physics-informed neural networks]{Physics-informed \\ neural networks} \label{ch:pinns}

The idea of utilizing neural networks to solve ordinary and partial differential equations is rather old, having been explored twenty and more years ago \cite{leeNeuralAlgorithmSolving1990,lagarisArtificialNeuralNetworks1998}. However, these methods were limited in their scope of application: not only have computational capacities improved manifold within the last two decades, but also the research on neural networks has progressed. Where in 1998, Lagaris \textit{et al.} had to analytically implement the derivatives of the model with respect to the spatial and temporal coordinates of the solution of the differential equation (where they used the sigmoid activation function for the hidden layer, which today is largely outdated) \cite{lagarisArtificialNeuralNetworks1998}, today, derivatives of any order can be calculated efficiently via automatic differentiation \cite{baydinAutomaticDifferentiationMachine2018a}.\par 
In this chapter, the motivation behind PINNs is explored and a simple PINN based on the function approximator for the solution of a 1D Poisson equation (see Chapter~\ref{sec:functionapprox}) is presented. Finally, common choices for the node architecture, optimizer, initialization and hyperparameters will be presented and modern research developments discussed.


\section{Idea and mathematics of PINNs} \label{sec:ideapinns}

Whatever the progress in deep learning, the main idea of Lagaris \textit{et al.} \cite{lagarisArtificialNeuralNetworks1998} has nonetheless persisted. Consider the differential equation of the form
\begin{equation} \label{eq:standardpde}
\frac{\pa u(t,\bx)}{\pa t} + \mathcal{N} [u(t,\bx), \lambda] = 0, \ \ \bx \in \Omega \subseteq \R^N, \ t\in \mathcal{T} \subseteq \R,
\end{equation}
with the solution $u(t,\bx)$ and the non-linear differential operator $\mathcal{N}$ with the coefficient vector $\lambda$. For ease of explanation, let us disregard the boundary $\partial\Omega$. The domains $\Omega$ and $\mathcal{T}$ are now discretized to a set of \textit{collocation points} $\hat{\Omega}$ and $\hat{\mathcal{T}}$. These describe nothing more than the points where the differential equation is aimed to be satisfied during training. They are denoted by a \textit{hat}-symbol. A trial solution $u_\mathrm{trial}(t_j,\bx_i,p)$ is introduced, where $p$ is a vector of adjustable parameters. The problem is thus transformed to
\begin{equation} \label{eq:minimizeresid}
\min_{p} \sum_{\bx_i \in \hat{\Omega}} \sum_{t_j \in \hat{\mathcal{T}}} \left( \frac{\pa u_\mathrm{trial}(t_j, \bx_i, p)}{\pa t_j} + \mathcal{N} [u_\mathrm{trial}(t_j, \bx_i, p), \lambda] \right) = 0, 
\end{equation}
which, if we identify the trial solution as the output of a feed-forward neural network and the parameter as the weights and biases, is ``just'' another minimization problem solvable by gradient descent techniques \cite{lagarisArtificialNeuralNetworks1998}.\par 
Essentially, we add the differential equation's residual to the cost function and minimize them both. This easily extends to the implementation of boundary and initial conditions; they correspond to two further residuals to be added to the cost function. This enables us to visualize PINNs as an extension of feed-forward neural networks, see the illustration in Fig.~\ref{fig:illustrationpinn} (inspired by Ref.~\citenum{karniadakisPhysicsinformedMachineLearning2021}). Note that while the feed-forward neural networks discussed in Chapter~\ref{ch:nns} constituted an approach of \textit{supervised} learning (where data is provided and the performance of the network is determined by comparison to a testing set), PINNs learn \textit{semi-supervised}: Firstly, data for the boundary and initial condition are provided in order to limit the solution space to just one particular solution. If available, experimental data or simulated data can also be provided. These constitute the supervised aspect of PINNs. However, by design, the actual minimization of the residual \eqref{eq:residual} constitutes an \textit{unsupervised} approach since no data has to provided at the collocation points. 

\begin{figure}[H]
	\centering
	\includegraphics{graphics/chapter3/pinn.pdf}
	\caption{Illustration of a physics-informed neural network for a 1D Poisson equation. A feed-forward neural network provides a prediction $u$ for a given number $x$. Through automatic differentiation, the derivatives are determined and the residual of the Poisson equation calculated. Put together with boundary data and -- if available -- external data for $u$, the loss is then calculated. Through an optimization technique (e.g. stochastic gradient descent), the weights are adjusted so that the loss, after a certain number of iterations, becomes minimal.}
	\label{fig:illustrationpinn}
\end{figure}


\paragraph{Notation} The left-hand-side of \eqref{eq:standardpde} will now be defined as $f(t,\bx)$, i.e.
\begin{equation} \label{eq:residual}
f \coloneqq \frac{\pa u}{\pa t} + \mathcal{N} [u,\lambda].
\end{equation}
When minimizing it, $f$ will also be called the \textit{residual} of the partial differential equation throughout this work. This terminology is unrelated to the residual used in traditional numerical methods such as finite element methods. \par 
We now differentiate between five types of input data, where the notation for the set size is now introduced: the training set $m_\mathrm{train}$, the testing set $m_\mathrm{test}$, the collocation points of the differential equation $m_{f}$, the collocation points at the boundary $m_{b}$ and the collocation points for the initial condition $m_\mathrm{init}$. These respective indices will from now on be used to differentiate between the sets.

\paragraph{Cost function} Where the cost function $J$ was previously given by, e.g., \eqref{eq:mse}, we now minimize \cite{raissiPhysicsinformedNeuralNetworks2019}
\begin{align}
J &= w_\mathrm{data} \cdot \mathrm{MSE}_\mathrm{data} + w_f \cdot \mathrm{MSE}_f \nonumber \\
& \ \ \ \ + w_b \cdot \mathrm{MSE}_b + w_\mathrm{init} \cdot \mathrm{MSE}_\mathrm{init} \label{eq:pinncost} \\
\intertext{where}
\mathrm{MSE}_\mathrm{data} &= \frac{1}{m_\mathrm{train}} \sum_{i=1}^{m_\mathrm{train}} | y_i - \hat{y}_i |^2,  \\
\mathrm{MSE}_f &= \frac{1}{m_f} \sum_{i=1}^{m_f} | f(t_i, x_i) |^2, \\
\mathrm{MSE}_b &= \frac{1}{m_b} \sum_{i=1}^{m_b} | y_b - \hat{y}_b |^2 \label{eq:msebound}
\intertext{and}
\mathrm{MSE}_\mathrm{init} &= \frac{1}{m_\mathrm{init}} \sum_{i=1}^{m_\mathrm{init}} | y_i - \hat{y}_i |^2
\end{align}
The individual $\mathrm{MSE}$ components are weighted with individual factors $w$. One could intuitively regard these as ``physics learning rates''. These weights, inspired by the collocation finite element method \cite{bochevLeastSquaresFiniteElement2009}, have been introduced by Nabian and Meidani for the purpose of improving convergence by balancing of the cost components \cite{nabianDeepLearningSolution2019}. There, they were chosen empirically. This approach will also be taken in this thesis. The role of the weights has been studied by Wang \textit{et al.}, where, inspired by the Adam optimizer, a rule for adaptively choosing the weights has been introduced and shown to improve convergence \cite{wangUnderstandingMitigatingGradient2021} (see Sections~\ref{subsec:hyperparameters} and~\ref{subsec:convergence}). \par 
For boundary conditions which make demands upon the derivatives of $u(t,\bx)$, such as Neumann conditions, \eqref{eq:msebound} -- albeit true -- is impractical to implement. In those cases, the initial condition is technically another differential equation which is thus similarily treated as \eqref{eq:standardpde}. To illustrate, take Neumann conditions on the boundary $\partial\Omega$
\begin{equation}
\frac{\partial u(t,\bx)}{\partial \bx} \biggl|_{\partial\Omega} = 0.
\end{equation}
Then, the left-hand-side will be defined as $f_b (t,\bx)$, $\bx\in\partial\Omega$, and subsequently, the cost function for the boundary condition is
\begin{equation}
\mathrm{MSE}_b = \frac{1}{m_b} \sum_{i=1}^{m_b} | f_b(t_i, \bx_i ) |^2, \ \ \bx_i \in\partial\hat{\Omega}.
\end{equation}



\section{Implementation of a simple PINN} \label{sec:pinnpoisson}

In the following, the neural network explored in Chapter~\ref{sec:functionapprox} is extended to a PINN. In Chapter~\ref{sec:functionapprox}, a sine function $u(x) = \sin(2\pi x)$ was approximated. Now, we not only provide the same training data for the sine function but also incorporate the corresponding 1D Poisson equation
\begin{equation} \label{eq:poisson1dpinn}
\ddxz u(x) + 4\pi^2 \sin(2\pi x) = 0,
\end{equation}
into the loss function. For graphical intuition for this PINN, see Fig.~\ref{fig:illustrationpinn}. \par 
The basis of the PINN is still the code presented in Chapter~\ref{sec:functionapprox}, except for the training step function, which needs to be modified. Again, the full code can be found at the \texttt{GitLab} repository, \url{https://gitlab.com/jakobbruhnke/1DPoisson}.\par 
As a first addition, the function which returns the differential equation's residual, \eqref{eq:poisson1dpinn}, is created. For higher-order derivatives, the \texttt{persistent=True} argument is required within the \texttt{GradientTape} scope in order for it to track the derivatives. 

\begin{lstlisting}[language=Python]
def physics_residual(model, x):
    with tf.GradientTape(persistent=True) as tape:
        tape.watch(x)
        u = model(x)
        dx = tape.gradient(u, x)
        
    dx2 = tape.gradient(dx, x)
    del tape 
    
    res = dx2 + 4 * np.pi**2 * tf.sin(2 * np.pi * x)
    return res
\end{lstlisting}

Next, the collocation points for the differential equation (\texttt{x\_{}physics}) and for the lower and upper boundaries are created. 

\begin{lstlisting}[language=Python]
n_colloc = 100
x_physics = tf.random.uniform((n_colloc, 1), -1., 1.)

x_lb = tf.convert_to_tensor([-1.])
x_ub = tf.convert_to_tensor([1.])
x_lb = tf.reshape(x_lb, [1, 1])
x_ub = tf.reshape(x_ub, [1, 1])
\end{lstlisting}

\newpage  % formatting

The weights introduced in \eqref{eq:pinncost} are defined as $w_\mathrm{data} = 1$, $w_f = \num{5e-4}$ and $w_b = 1$. If one were to not assign weights at all, i.e. $w_f = 1$, this author found a performance reduction by a factor of approximately six.

\begin{lstlisting}[language=Python]
data_weight = 1
res_weight = 5e-4
bound_weight = 1
\end{lstlisting}

The new training step function is created analogously to Chapter~\ref{sec:functionapprox}. The new cost is calculated based on \eqref{eq:pinncost}. Please note that as optimizer, only Adam is used. For such a simple PINN, Adam can yield satisfactory results. For more complicated PINNs, subsequent optimization with the second-order method L-BFGS is essential and will be done in Chapters~\ref{ch:pinngaussianwave} and~\ref{ch:pinnwaveguide}.

\begin{lstlisting}[language=Python]
def train_step(model, x_train, u_train, x_physics, x_lb, x_ub):
    with tf.GradientTape() as tape:
        tape.watch(model.trainable_weights)
        
        u_predict = model(x_train, training=True)
        u_predict_lb = model(x_lb, training=True)
        u_predict_ub = model(x_ub, training=True)
        res = physics_residual(model, x_physics)
        
        nn_cost = data_weight * loss_function(u_train, u_predict)
        res_cost = res_weight * tf.reduce_mean(tf.square(res))
        bound_cost = bound_weight * (
        			   tf.reduce_mean(tf.square(u_predict_lb))
                     + tf.reduce_mean(tf.square(u_predict_ub)) )
        cost = nn_cost + res_cost + bound_cost
    
    grads = tape.gradient(cost, model.trainable_weights)
    optimizer.apply_gradients(zip(grads, model.trainable_weights))
    return cost
\end{lstlisting}

By iteratively calling the \texttt{train\_{}step} function, the PINN is now trained analogously to a ``standard'' neural network such as the one introduced in Chapter~\ref{sec:functionapprox}.

\paragraph{Results} After $2148$ iterations, the cost has decreased to $\num{1e-4}$. The cost dynamics including the isolated contributions by data, residual and boundary cost are depicted in Fig.~\ref{fig:pinnpoisson_cost}. Note that $\mathrm{MSE}_f$ decreases monotonously; it does not contain the same spikes as seen for $\mathrm{MSE}_\mathrm{data}$ or $\mathrm{MSE}_b$. However, this behaviour, it has to be emphasized, is not a characteristic of the residual cost. For PINNs discussed later on, the cost dynamic of the partial differential equation's residual is hardly as ``smooth'' as seen here.

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter3/loss_epoch_pinn.pdf}
\caption{Development of the total cost and its components (cost of the training set, $\mathrm{MSE}_\mathrm{data}$, residual cost, $\mathrm{MSE}_f$, and boundary cost, $\mathrm{MSE}_b$) throughout the training process of the PINN. Confirming intution, $\mathrm{MSE}_b$ displays the most erratic behavious since with only two datapoints it is most sensitive to fluctuations.}
\label{fig:pinnpoisson_cost}
\end{figure}


Compare now the results of the neural network of Chapter~\ref{sec:functionapprox}, depicted in Fig.~\ref{fig:poisson_u}, with the results of the physics-informed neural network, given in Fig.~\ref{fig:pinnpoisson_u}. 

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter3/trainingprocess_pinn.pdf}
\caption{Results of the PINN tracked over $200$, $500$, $1000$ and $2147$ training cycles, where the training data (blue, circles), exact solution (red, dotted) and the model's prediction (blue, solid) are compared. }
\label{fig:pinnpoisson_u}
\end{figure}

While the start is rather similar, already in the $500$th iteration, we see that $u(x)$ curves downwards, thus decreasing the cost due to the differential equation's residual. By the $1000$th iteration, it has adopted the general shape of the function to be approximated and after $2100$ iterations, the deviations to the exact solution are so small as to be visually undetectable. Note that the boundary condition is in all four snapshots satisfied. This is a general observation, no matter how complicated the PINN is: PINNs learn faster on explicit data than on implicit penalization of solutions not fulfilling the differential equation. Thus, even small amounts of experimental data provided to a PINN may be benefitial for performance and convergence.



The relative $\L_2$ error, 
\begin{equation}
\L_2 = \frac{\| y - \hat{y} \|_2}{\| \hat{y} \|_2} = \frac{\sqrt{(y_1 - \hat{y}_1)^2 + ... + (y_n - \hat{y}_n)^2}}{\sqrt{(\hat{y}_1)^2 + ... + (\hat{y}_n)^2}}  ,
\end{equation}
a common metric used to evaluate PINN performance, is in this case given by $\num{6.4e-3}$. Training was performed on an Intel Core i7-6600U\footnote{2 cores, 4 threads, $\SI{2.60}{\giga\hertz}$ base frequency} and took $\SI{36}{\s}$. Note that for these small array sizes, a CPU is faster than a GPU due to the higher base frequency. For 2D calculations, such as those performed within the next two chapters, a GPU will be employed for cases where single precision (\texttt{float32}) is sufficient.\footnote{Usually, neural networks are always trained with \texttt{float32} since GPUs cannot handle double precision. As will be discussed later, for PINNs utililzing the L-BFGS optimization routine, it can be beneficial to switch to \texttt{float64}.} \par 
It is interesting to compare the relative $\mathcal{L}_2$ error with the cost dynamics depicted in Fig.~\ref{fig:pinnpoisson_cost}. This analysis can be seen in Fig.~\ref{fig:pinnpoisson_l2_and_cost}. It is evident that, while the relative $\L_2$ error and cost correlate in the early training stages and for the sudden spikes, there is one glaring difference: despite the cost -- ignoring the spikes -- decreasing monotonously, the $\L_2$ error has a minimum at approximately $1600$ iterations; afterwards it increases. This may be an example of overfitting, see Chapter~\ref{subsec:overfitting}. Theoretically, corresponding to the early stopping method (Box~\ref{bx:regularization}), one could therefore stop the training process at $1600$ iterations in order to achieve the lowest error. However, the promise of PINNs is the application to cases where the exact solution is unknown and thus the $\L_2$ error not determinable. It is therefore not in the spirit of PINNs to employ this regularization method.

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter3/loss_and_l2.pdf}
\caption{Comparison of the total cost and the relative $\L_2$ error during the training process.}
\label{fig:pinnpoisson_l2_and_cost}
\end{figure}

Also, it must be added that this PINN models a very basic problem and is thus more prone to overfitting. For differential equations with high frequency components, as examined in later sections of this thesis, underfitting is a more pressing issue.



\section{Modern developments} \label{sec:moderndev}

There have been several systematic studies on the behaviour of PINNs with respect to the layer number, node-per-layer number, number of collocation points, optimization routines, activation functions, weights of the cost function and convergence properties. 
Some of these findings have been summarized by %Markidis \cite{markidisOldNewCan2021},
Karniadakis \textit{et al.} \cite{karniadakisPhysicsinformedMachineLearning2021}, Blechschmidt \textit{et al.} \cite{blechschmidtThreeWaysSolve2021} and Cuomo \textit{et al.} \cite{cuomoScientificMachineLearning2022}.\par 
PINNs are a highly active research topic. New techniques, new variants and new applications are reported on constantly. It is beyond the scope of this thesis to provide a comprehensive overview on these \textit{state-of-the-art} developments. In the following, a small subset of these findings shall be discussed: optimal hyperparameters and convergence properties.

\subsection{Hyperparameters} \label{subsec:hyperparameters}

In Tab.~\ref{tab:impact_hyperparameters}, the available hyperparameters of a PINN are explored and their impact on the performance of PINNs discussed.

\subsection{Convergence properties} \label{subsec:convergence}

While not extensive, there has been some research on convergence properties of PINNs. This includes mathematical discussions on convergence properties \cite{shinConvergencePhysicsInformed2020} of various types of partial differential equations as well as development of methods which improve convergence or, for certain PDEs, even enable it where, with conventional PINNs, no convergence is achieved. Reliable and predictable convergence is a prerequisite for PINNs to become reliable methods in the computational physics community. Therefore, a somewhat longer discussion on the convergence properties of PINNs may be of interest to the reader.


\paragraph{Adaptively choosing weights of loss components} One of the most simple methods available in the toolkit for PINNs are the weights of the cost components introduced by Nabian and Meidani \cite{nabianDeepLearningSolution2019}. This method was also utilized in this thesis for the 1D Poisson equation, \eqref{eq:poisson1dpinn}. As mentioned there, this author found that convergence speed reduces by a factor of $6$ if the weight of the differential equation's residual does not differ from unity.\par 
Wang \textit{et al.} have furthermore, inspired by the Adam optimizer, proposed a \textit{learning rate annealing} algorithm for adaptively balancing the weights during the training process \cite{wangUnderstandingMitigatingGradient2021}. For the Helmholtz equation, this algorithm improves the relative $\L_2$ error by more than an order of magnitude.


\begin{table}[H]
    \centering
    \caption{Impact of hyperparameters on the performance of PINNs. Note that the definition of hyperparameter for this table can be reduced to ``anything which is pre-determined by the user'' and may in its scope be broader than usual (for example, the choice of the optimizer is not usually denoted as a hyperparameter).}
    \label{tab:impact_hyperparameters}
    \rowcolors{1}{tableblue}{White}
    \begin{tabularx}{.95\textwidth}{p{.2\textwidth}X}
    \arrayrulecolor{darktableblue} \hline 
    Hyperparameter & Impact on PINN performance \\ \hline
    No. of layers & Too low: erroneous convergence \cite{markidisOldNewCan2021}. Usually ``the more, the better'', however, high $L$ increase computational cost dramatically. \\
    Nodes per layer & Similar to number of layers. It has been observed that it can be beneficial to place a low number of nodes at the early layers and a higher number at the high layers. A possible explanation is that the early layers are responsible for low-frequency components of the solution \cite{markidisOldNewCan2021}. \\
    No. of collocation\newline points &  Too low: erroneous convergence \cite{cuomoScientificMachineLearning2022}. ``The more, the better'' -- however, computational cost increases significantly. \\
    Sampling of\newline colloc. points & Stochastic sampling methods are used. Quasi-random sequences (Latin Hypercube Sampling, Sobol) may boost performance slightly \cite{blechschmidtThreeWaysSolve2021}.  \\
    Weights &  Weighing of loss components is highly impactful. Besides manual, empirically motivated choice, there has also been suggested an adaptive rule \cite{wangUnderstandingMitigatingGradient2021} (see Section~\ref{subsec:convergence}).  \\
    Optimizer & Adam with subsequent L-BFGS is state of the art \cite{raissiPhysicsinformedNeuralNetworks2019, markidisOldNewCan2021}. \\
    Activation\newline function &  Commonly used are $\tanh$ and \textit{swish} \cite{cuomoScientificMachineLearning2022}. Locally Adaptive Activation Functions (LAAF) are a promising technique to improve performance \cite{markidisOldNewCan2021}. \\
    Loss function & Mean squared error ($\mathrm{MSE}$) is commonly used \cite{kollmannsbergerPhysicsInformedNeuralNetworks2021}. There have been developments where the boundary conditions are provided to the PINN as \textit{hard constraints} by encoding them directly in the architecture \cite{sunSurrogateModelingFluid2020}. This improves performance.  \\
    Initialization & If possible, transfer learning should be used \cite{markidisOldNewCan2021} in order to initialize with the weights of a trained PINN (see next section). Otherwise, Glorot initialization \cite{glorotUnderstandingDifficultyTraining2010} is the most common choice. \\ \hline
    \end{tabularx}
\end{table}




\paragraph{Transfer learning} Transfer learning is an important tool in the deep learning community when training speed is a bottleneck in the development process \cite{zhuangComprehensiveSurveyTransfer2021}. Thus, it is not surprising that it has also been utilized for PINNs \cite{markidisOldNewCan2021}. The idea behind transfer learning is rather intuitive: if one implements a neural network for solving a particular problem, one seldomly ``reinvents the wheel''. In order to speed up the training process, information of prior trained models is ``transferred'' to the newly set-up neural network.\par 
Applied to PINNs, this is commonly achieved by initializing the \textit{to-be-trained} model with the weights of a PINN approximating a similar problem. While generally improving training speed, transfer learning can even enable training for PINNs which fail to converge with uninformed initialization (e.g. Glorot initialization) \cite{krishnapriyanCharacterizingPossibleFailure2021}. In those cases, several models with increasing complexity (e.g. more components of higher frequency) are trained in succession, where the weights of the previous model are provided as initialization to the current. This gradual increase of complexity is commonly referred to as \textit{curriculum learning} \cite{wangSurveyCurriculumLearning2021, krishnapriyanCharacterizingPossibleFailure2021}.


%-- instead of starting with a problem of the highest complexity, it is most natural to gradually increase complexity. For PINNs, this may mean that one first considers a linear partial differential equations before introducing non-linearities. Applied to this particular example, transfer learning entails supplying the non-linear PINN with information of the already trained, linear PINN.\par 
%This is achieved by transferring the weights of the linear problem to the non-linear problem. Specifically, if for the linear problem, four hidden layers were utilized, these ``fully trained'' four hidden layers are chosen as hidden layers for the non-linear equation and, e.g., two more hidden layers added. During training of the non-linear problem, only the ``new'' hidden layers are optimized while the four hidden layers, optimized for the linear equation, are fixed.\par 
%This approach has proven useful for PINNs. For example, \textbf{Author} has examined the effects of transfer learning on convergence speed and found that it has the potential to increase the speed by factors of up to \textbf{adsf} for the PINNs implemented therein.

\begin{comment}
\paragraph{Guaranteed convergence} \textcolor{red}{this paragraph is mathematically highly dubious (i.e. wrong) and needs to be fixed} Shin, Darbon and Karniadakis have shown that the \textbf{Hölder norm} of parabolic and \textbf{[type]} equations converges \textit{given sufficient expressivity of the neural network architecture, i.e. enough hidden layers and nodes per layer} \cite{shinConvergencePhysicsInformed2020}. It has to be emphasized that the Hölder norm is not commonly used as a loss function. Empirically, the authors observed that in the cases where the Hölder norm  is guaranteed to converge, the $\L_2$ norm converges with much greater speed, albeit not monotonously.\par 
While these remarks may seem superfluous, this particular finding is of interest to the topics of this thesis -- the paraxial wave equation for a quadratic-index medium, \eqref{eq:paraxialwaveeq_n}, is, by nature, a parabolic differential equations and we can thus expect a PINN to succeed in satisfactorily approximating a global solution.
\end{comment}


\paragraph{Where PINNs fail to train} There have been several reports of differential equations which cannot, by conventional PINNs, be solved.\footnote{When speaking of failure, it is assumed that the available hyperparameters (number of collocation points, hidden layers, nodes per layers) have been chosen wisely.} Reasons for this failure can be manifold. \par 
A. S. Krishnapriyan \textit{et al.} have observed convergence difficulties in differential equations with an adjustable parameter $k$, where $k$ controls the intricacy of the solution \cite{krishnapriyanCharacterizingPossibleFailure2021}. For certain $k$ -- often high values, corresponding to high frequencies -- a global solution can sometimes not be approximated. One example of such an equation is the convection problem in one spatial dimension.
\begin{equation} \label{eq:convection}
\ppt u + k \ppx u = 0, \ \ \ k = 30
\end{equation}
In explaining this behaviour, A. S. Krishnapriyan \textit{et al.} examined the multidimensional loss surface and found that for low $k$, the loss surface is rather smooth mostly while for high $k$, the surface becomes increasingly more complex to optimize (e.g. plateaus of vanishingly small gradients and sudden spikes in the loss) \cite{krishnapriyanCharacterizingPossibleFailure2021}. In making such PDEs accessible for neural networks, they propose two methods: \textit{curriculum regularization} (equivalent to \textit{curriculum learning}, see the paragraph on transfer learning) and \textit{sequence-to-sequence learning}.

%\paragraph{Curriculum regularization} The idea behind curriculum regularization is conceptually similar to curriculum learning, which is widely used in research and applications of deep learning. The training process, as in transfer learning, again proceeds from ``simple'' to ``complicated''. In contrast to transfer learning however, the neural network architecture is kept constant between the simple and complex problem while one parameter -- $k$, governing the complexity -- is increased from one PINN to the next while the weights of the $(n+1)$-th PINN are initialized as the trained weights of the $n$-th PINN. This approach has enabled A. S. Krishnapriyan \textit{et al.} to train a PINN on the convection problem, \eqref{eq:convection}, with $k=30$.


\paragraph{Sequence-to-sequence learning} This approach is based on the fact that PINNs most optimally train in regions where actual data is provided -- this being the boundaries. Sequence-to-sequence learning leverages this property by subdividing the spatial or temporal region on which the differential equation is defined and successively training PINNs on the separate regions. This approach is illustrated in Fig.~\ref{fig:seq2seq}. One first trains on the region where the initial solution is specified. Subsequently, the PINN on the bordering region is supplied as initial solution the approximated solution of the neighbouring PINN. This process is iterated until the differential equation is cummulatively solved by all PINNs. With this method, A. S. Krishnapriyan \textit{et al.} have found an accurate solution for the reaction-diffusion system \cite{krishnapriyanCharacterizingPossibleFailure2021}
\begin{equation}
\ppt u - \nu \ppxz u - \rho u(1-u) = 0, \ \ \ \nu = \rho = 5.
\end{equation}

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter3/seq2seq.pdf}
\caption{Illustration of the sequence-to-sequence learning approach. Left: collocation points (red: initial condition, yellow: boundary) used for finding a global solution. Right: only a fifth of the temporal region is used for the first PINN. Subsequent PINNs take as initial condition the boundary solution of its predecessor.}
\label{fig:seq2seq}
\end{figure}


\section{Summary}

The discussion on convergence properties of PINNs marks the end of the theoretical chapters of this thesis. Thus, before PINNs for the Gaussian beam and parabolic waveguide are presented in Chapters~\ref{ch:pinngaussianwave} and~\ref{ch:pinnwaveguide}, a short summary is due in order to motivate the following calculations.\par 
The first physics-informed neural network was presented by Raissi \textit{et al.} in $2017$ in two preprints \cite{raissiPhysicsInformedDeep2017a, raissiPhysicsInformedDeep2017}. The first peer-reviewed study on PINNs was subsequently published by Raissi \textit{et al.} in 2019 \cite{raissiPhysicsinformedNeuralNetworks2019}. The motivation behind PINNs is to encode the differential equation and boundary as well as initial conditions as additional regularization terms in the loss function, \eqref{eq:pinncost}. While the idea of using feed-forward neural networks as solvers for partial differential equations is not new \cite{lagarisArtificialNeuralNetworks1998}, efficient implementation has only recently been enabled due to progress in automatic differentiation.\par 
In Section~\ref{sec:pinnpoisson}, a PINN for a simple 1D Poisson problem was presented and its implementation in \texttt{TensorFlow} shown and explained. The code presented can easily be extended to more complicated problems and did in fact serve as a basis for the PINNs presented in Chapter~\ref{ch:pinngaussianwave}. For the 1D Poisson equation, many attributes of PINNs can already be observed:
\begin{itemize}
\item PINNs train best on explicit data (boundary / initial condition; experimental data).
\item $\MSE_f$ is generally the dominant cost contribution.
\item At some point during the training process, there is a steep decline of $\MSE_f$ after which training -- while still necessary -- becomes slow.\footnote{While not shown for the 1D Poisson PINN, it is this very decline after which optimization with L-BFGS becomes viable. Employed earlier, the L-BFGS method, being a second-order method, may lead to erroneous convergence.}
\item Due to the stochastic nature of optimization with Adam, the cost spikes irregularily but generally goes back down swiftly. These cost spikes are most notable for boundary condition, $\MSE_b$.% (and later initial conditions, $\MSE_\mathrm{init}$).
\end{itemize}
Notably, overfitting may have occured in the training process, i.e. the relative $\L_2$ error increased despicte a decrease in cost. Due to the regularizing nature of PINNs, this is untypical. However, it will not be discussed further since it was not observed for the PINNs presented in the following chapters.\par 
Lastly, modern developments were presented. First, optimal hyperparameters were discussed. Due to the limited scope of this thesis, not all \textit{state-of-the-art} methods will be employed in Chapters~\ref{ch:pinngaussianwave} and~\ref{ch:pinnwaveguide}. Deviances from \textit{state-of-the-art} methods are listed in the following:
\begin{itemize}
\item The sampling of collocation points is done with random numbers from a uniform distribution rather than with space-filling quasi-random sequences.
\item The weighing of cost components is done manually rather than with adaptive rules.
\item Only $\tanh$ as activation function is used.
\item The boundary conditions are only given as \textit{soft constraints}, i.e. encoded into the cost function rather than the neural network architecture.
\end{itemize}
Secondly, the convergence of PINNs was examined in more detail. Research on convergence properties of PINNs is still in the early stages. What has been proven to work well for improving convergence are, among others, adaptive rules for learning rates, curriculum learning and sequence-to-sequence learning. While I have successfully employed the latter two methods, these results have, in the end, not been included in this thesis. Still, I would like to emphasize the benefits of curriculum learning. Due to the general workflow in implementing PINNs, where one often starts at ``easy'' problems before moving to complicated cases, curriculum learning may be the most intuitive and simplest method which, at extremely low effort, can yield great results.