\chapter[Solving optical problems with PINNs]{Solving optical problems \\ with PINNs} \label{ch:opticswithpinns}

By now, we have explored Gaussian beam propagation and studied the parabolic wave guide in Chapter~\ref{ch:optics}, delved into the basic theory and intuition behind feed-forward neural networks in Chapter~\ref{ch:nns} and examined PINNs as an extension of these networks in Chapter~\ref{ch:pinns}. With the underlying theory being adequately adressed, this Chapter will build upon these foundations in order to present PINNs solving the paraxial Helmholtz equation in free space and paraxial wave equation for a quadratic-index medium.\par 
First, propagation of Gaussian beams will be explored in Section~\ref{sec:pinngaussianwave}. Gaussian beams, being a good model for perfect laser light, are of paramount importance in today's optics. In order to transmit such laser light (almost) without loss, wave guides have been used in optics for more than a century. The parabolic wave guide, being the topic of Section~\ref{sec:pinnwaveguide} is, it has to be noted, not a realistic wave guide. In a parabolic wave guide, all modes are guided since the refractive index diverges for high $x$, $n(x) \rar \infty, \ x \rar \infty$. Still, for small $x$, it can serve as a valuable model system in which guided wave propagation can be explored since a closed-form analytical solution in the form of Hermite-Gaussian modes exists. A more realistic example for a wave guide would be the case of a Gaussian index profile. Here, light of higher modes will escape and thus, transmission will not be lossless.



\section{Propagation of a Gaussian beam in free space} \label{sec:pinngaussianwave}

In this section, various PINNs solving the paraxial Helmholtz equation for a Gaussian distribution as initial condition will be explored. Several aspects of these PINNs will be explored:
\begin{itemize}
\item What is the impact of the boundary condition? 
\begin{itemize}
\item Can back reflections be seen for Dirichlet boundary conditions?
\end{itemize}
\item How does training differ between two normalizations which govern how complicated the solution is?
\item What are the extrapolation capabilities?
\end{itemize}

\subsection{Setting up the two-dimensional problem}

One important particular solution of the paraxial Helmholtz equation is the Gaussian beam $A(\br)$, \eqref{eq:gaussianwaves}. Naturally, one can set up a PINN to solve this three-dimensional problem. % and indeed, this has been tried so some success by the author of this thesis. 
However, more dimensions are detrimental to the speed of convergence. On high-performance clusters, the training speed of this particular problem in 3D will presumably still be reasonable (in the regime of several hours). However, since the PINN here presented is to be understood as a ``toy problem'' set up in order to provide a suitable introduction to the paraxial wave equation of a quadratic index medium (which will be treated in 2D, $x$- and $z$-dimensions only), it is sufficient for the scope of this thesis to solve the paraxial Helmholtz equation in free space in two dimensions, $x$ and $z$, only.

\paragraph{Dimensionality reduction of the paraxial wave equation} The paraxial wave equation is given by \eqref{eq:paraxialhelmholtz_vac}. In 2D we obtain
\begin{equation} \label{eq:paraxialhelmholtz_vac_2d_ch4}
\ppxz A(x,z) + 2\i k \ppz A(x,z) = 0.
\end{equation} 
Its most well-known solution is the Gaussian wave, \eqref{eq:gaussianwaves}, which in two dimensions is given by
\begin{equation} \label{eq:gaussianwaves_2D}
A(x,z) = \frac{A_0}{\sqrt{q(z)}} \exp\left[\i k\frac{x^2}{2q(z)}\right],
\end{equation}
$q(z) = z - \i z_0$, simply obtained by factorizing $A(x,y,z) = A(x,z) A(y,z)$.

\paragraph{Real and imaginary components} Both the solution as well as the differential equation itself are complex. While \texttt{tensorflow} allows for complex datatypes, this is not mirrored by other machine learning libraries. In most publications on PINNs, only real datatypes for neural networks are utilized and this thesis is no different in this respect.\par  %While there are current developments to enable better compatibility of complex datatypes with machine learning frameworks such as \texttt{tensorflow} \textbf{SOURCE - arXiv:2009.08340v2}, in this work, for ease of compatibility with other libraries, only real datatypes will be utilized.\par 
Thus, the complex-valued PDE has to be split up into two coupled differential equations for the real and imaginary part. Let
\begin{equation}
A(x,z) \coloneqq u(x,z) + \i v(x,z).
\end{equation}
Substitution into \eqref{eq:paraxialhelmholtz_vac_2d_ch4} yields two coupled differential equations forming the basis for the PINN,
\begin{align}
\frac{\partial^2 }{\partial x^2} u(x,z) - 2k\partial_z v(x,z) &= 0 \\
\frac{\partial^2 }{\partial x^2} v(x,z) + 2k\partial_z u(x,z) &= 0
\end{align}
One can easily give explicit expressions for $u(x,z)$ and $v(x,z)$. However, this is hardly necessary for most use-cases. If one desires to plot $u$ and $v$ in any programming language capable of dealing with complex datatypes (such as \texttt{Python} and its array library \texttt{numpy}), it is most natural to create a complex-valued function \texttt{A(x,z)} and then calculate \texttt{u = real(A(x,z))} and \texttt{v = imag(A(x,z))}.

\paragraph{Boundary conditions} The extent of the boundary, $x = x_b$, will be varied in the upcoming studies. We will examine both Dirichlet boundary conditions
\begin{align}
A(-x_b, z) = A(x_b, z) = 0
\end{align}
as well as symmetric boundary conditions
\begin{align}
A(-x_b, z) &= A(x_b, z) \\
\ppx A(x,z) \biggl|_{-x_b} &= \ppx A(x,z) \biggl|_{x_b}.
\end{align}

\paragraph{Normalization} Using actual physical units (such as $\si{\nano\meter}$ for the wavelength) is impractical for computational tasks due to floating point errors and the high computational cost attributed to increased floating point precision. Thus, the solution and PDE have to be normalized. For the Gaussian beam, it is natural to normalize $x$ with the beam width $w_0$ and $z$ with the Rayleigh length $z_0$. For these normalized coordinates $\xi = x / w_0$ and $\zeta = z / z_0$ (in subsequent text also referred to as the ``\textit{first normalization}''), we receive as PDE
\begin{align}
\ppxiz A(\xi, \zeta) + 4\i \ppze A(\xi, \zeta) = 0
\end{align}
and as solution
\begin{align} \label{eq:gaussnorm1}
A(\xi,\zeta) = \frac{A_0}{\sqrt{z_0 (\zeta - \i)}} \exp\left[\i \frac{\xi^2}{\zeta - \i}\right].
\end{align}
Similarily, the boundary value is now denoted as $\xi = \xi_b$. This normalization will be used first to show the equivalence of Dirichlet and symmetric boundary conditions in cases where $A(\xi, \zeta) \rar 0, \ \xi \rar \xi_b$. \par 
Later on, another normalization (in subsequent text also referred to as the ``\textit{second normalization}'') will be used, where $\xi' = \frac{x}{10w_0}$ and $\zeta' = \frac{z}{20z_0}$, where the PDE is given by
\begin{align}
\frac{1}{5} \ppxisz A(\xi', \zeta') + 4\i \ppzes A(\xi', \zeta') = 0
\end{align}
and the solution by
\begin{align} \label{eq:gaussnorm2}
A(\xi',\zeta') = \frac{A_0}{\sqrt{z_0 (20\zeta' - \i)}} \exp\left[\i \frac{(10\xi')^2}{20\zeta' - \i}\right].
\end{align}
Note that this second normalization is equivalent to ``zooming out'' of the first normalization. Thus, $A(\xi', \zeta')$ for the same extent of the boundaries, is more complicated and has more high-frequeny components than $A(\xi, \zeta)$. \par 
For both normalizations, we choose
\begin{equation}
\frac{A_0}{\sqrt{z_0}} = 1
\end{equation}
for simplicity and ease of training. As initial condition, $A(\xi,\zeta=0)$ or analogously $A(\xi', \zeta'=0)$ will be provided to the PINN.


\paragraph{Hyperparameters and other settings} In Tab.~\ref{tab:hyperparameter_gaussian}, the hyperparameters which remain constant throughout this section are provided, together with other settings.

\begin{table}[H]
    \centering
    \caption{Listing of the hyperparameters and chosen settings which all PINNs presented in Section~\ref{sec:pinngaussianwave} have in common.}
    \label{tab:hyperparameter_gaussian}
    \rowcolors{1}{tableblue}{White}
    \begin{tabularx}{.95\textwidth}{lcX}
    \arrayrulecolor{darktableblue} \hline 
    Hyperparameter & Symbol & Used for upcoming calculations \\ \hline
    Number of layers & $L$ & $\num{5}$ \\
    Nodes per layer & $n_l$ & $\num{32}$ for all layers \\
    Adam learning rate & $\alpha$ & $\num{1e-3}$  \\
    Collocation points (PDE) & $m_f$ & $\num{10000}$   \\
    Collocation points (Bound.) & $m_b$ & $\num{200}$  \\
    Collocation points (Init.) & $m_\mathrm{init}$ & $\num{200}$  \\
    Sampling of colloc. points & -- & Random numbers from uniform distribution \\
    Weight factor (PDE) & $w_f$ & First normalization, $A(\xi,\zeta)$: $\num{5e-4}$, \newline second normalization, $A(\xi', \zeta')$: $\num{1e-3}$  \\
    Weight factor (boundary) & $w_b$ & Symmetric boundary: $0.5$, \newline Dirichlet boundary: $1$ \\
    Weight factor (init.) & $w_\mathrm{init}$ & $\num{1}$  \\
    Optimizer & -- & In succession: Adam and L-BFGS  \\
    Activation function & $\mathrm{act}$ & $\tanh$ \\
    Loss function & -- & Mean squared error ($\mathrm{MSE}$) \\ \hline
    \end{tabularx}
\end{table}

\subsection{Training results}

\paragraph{Solving the paraxial wave equation with symmetric boundary conditions}

First, $A(\xi, \zeta)$ (first normalization, as in \eqref{eq:gaussnorm1}) was approximated by a PINN while enforcing symmetric boundary conditions with $\xi \in [-6, 6]$ and $\zeta \in [0, 2]$. Glorot initialization was used. Training was completed after $\num{15000}$ iterations of whom $\num{12000}$ were performed with the Adam optimizer and the last $\num{3000}$ with L-BFGS. Trained on a small GPU (NVIDIA GeForce 940MX), training took approximately $\SI{40}{\min}$. Of those, Adam optimization took $\SI{17}{\min}$ and L-BFGS $\SI{23}{\min}$, demonstrating the increased computational cost of second-order methods.

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_gaussian_periodic/int_gauss_compare_all_calc20}
\caption{Comparison of the model predictions (index $\mathrm{pred}$, \textit{left column}) with the exact analytical solution (index $\mathrm{ex}$, \textit{middle column}) and the errors $\Delta \beta = \left| \beta_\mathrm{pred} - \beta_\mathrm{ex}\right|$, $\beta \in \{u, v, |A|^2\}$ (\textit{right column}). The first row shows this analysis for $u(x,z)$, the second row for $v(x,z)$ and the third row for $|A(x,z)|^2$, where $A(x,z) = u(x,z) + \i v(x,z)$ is the complex amplitude \eqref{eq:gaussianwaves_2D}.}
\label{fig:gaussianperiodicsmall}
\end{figure}

The results are depicted in Fig.~\ref{fig:gaussianperiodicsmall}. Visually, comparing the left and middle columns (predictions and exact solution respectively), good agreement can be observed. It can be concluded that the PINN architecture is expressive enough for the task. Notably though, the regions of high error (Fig.~\ref{fig:gaussianperiodicsmall}, c) and f)) seem systematic in the sense that they appear as periodic stripes along the $z$ axis. Also, the relative $\L_2$ errors are of magnitude $\num{1e-2}$. While acceptable, this error is not ideal. In the literature, errors between $\num{1e-4}$ and $\num{1e-3}$ have been reported \cite{raissiPhysicsinformedNeuralNetworks2019}. It is likely that performance can be greatly improved by systematically optimizing for parameters such as the layer size $L$, number of nodes per layer $n_l$, collocation points $N_{f/b/\mathrm{init}}$ or weight factors $w_{f/b/\mathrm{init}}$.\par
Significantly, the error is not particularily big at the boundaries (this observation also holds for upcoming PINNs). High errors at the boundaries are commonly observed for standard PINNs. They originate from \textit{vanishing gradient pathologies} \cite{wangUnderstandingMitigatingGradient2021, markidisOldNewCan2021} and can be adressed by locally adaptive activation functions \cite{jagtapLocallyAdaptiveActivation2020}. \par
Given the intensity $|A(x,z)|^2$ predicted by the model, the beam width can be obtained by fitting a Gaussian onto the transverse beam profile at each $z$ value. For the normal distribution $f(x)$, parametric for $z$,
\begin{equation}
f(x;z) = \frac{A}{\sqrt{2\pi} \sigma(z)} \e^{-\frac{x^2}{2\sigma(z)^2}},
\end{equation}
the beam width $w(z)$ can easily be obtained:
\begin{equation}
w(z) = 2\sigma(z).
\end{equation}
The resulting beam width is shown in Fig.~\ref{fig:gaussianperiodicsmall_beamwidth}. It can be concluded that the PINN is qualitatively able to predict the beam dynamics. The predicted beam width is for higher $z$ values slightly smaller than the exact value. This may be an indicator that the PINN has not converged fully since during the training process, the beam broadening is learned during later training stages -- for the first $\approx \num{1000}$ iterations, the PINN usually predicts a collimated beam. This behaviour during training was observed by this author for PINNs solving various differential equations: if the boundary condition allows, the PINN starts by taking the ``easy path'' which, in this case corresponds to a function constant for all $z$ values -- i.e. a collimated beam. Subsequently, the general shape of the function is learned until at later stages, helped in part by switching the optimizer to L-BFGS, more intricate details are revealed.

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_gaussian_periodic/beam_width_gaussian_calc20}
\caption{Comparison of the exact beam width $w(z)$, \eqref{eq:beamwidth}, with the model prediction.}
\label{fig:gaussianperiodicsmall_beamwidth}
\end{figure}

While the previous PINN was successful, it has to be emphasized that the function to be approximated was of low complexity. A larger challenge is the approximation of $A(\xi', \zeta')$ (second normalization, as in \eqref{eq:gaussnorm2}) where $\xi' \in [1, 1]$ and $\zeta' \in [0, 1]$. Choosing these parameters corresponds to ``zooming out'' of the region approximated in Fig.~\ref{fig:gaussianperiodicsmall}.\par 
Training was again completed after $\num{15000}$ iterations ($\num{12000}$ Adam, $3000$ L-BFGS).

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_gaussian_periodic/int_gauss_compare_all_calc16}
\caption{Comparison of model predictions, exact analytical solution and the error with definitions analogous to Fig.~\ref{fig:gaussianperiodicsmall}. The secondly discussed normalization with $\xi' = x / 10w_0$ and $\zeta' = z / 20z_0$ is employed with $\xi' \in [-1,1]$ and $\zeta' \in [0,1]$.}
\label{fig:gaussianperiodicbig}
\end{figure}

The results for the second normalization are shown in Fig.~\ref{fig:gaussianperiodicbig}. Mostly, good agreement is achieved. The errors are slightly higher but, judging by the increased number of complicated features, this was to be expected to some degree. The same periodicity of regions of high error can be observed. The source for these errors can be better visualized by taking a transverse slice of $A(\xi', \zeta') = u(\xi', \zeta') + \i v(\xi', \zeta')$ at $\zeta' = 0.4$, corresponding to $z = 8z_0$. These transverse beam profiles are depicted in Fig.~\ref{fig:gaussianperiodicbig_slice}. \par 
We see that the PINN struggles to reach the maxima and minima of the side-oscillations with higher frequency and lower amplitude. To some degree, it is to be expected that these features are the most difficult to approximate -- PINNs generally tend to struggle with multi-scale features (although the difference in scale for the side-oscillations is rather mild).

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_gaussian_periodic/mid_solution_calc16.pdf}
\caption{Transverse beam profiles for $u(\xi', \zeta' = 0.4)$ and $v(\xi', \zeta' = 0.4)$, taken from Fig.~\ref{fig:gaussianperiodicbig}, where $\zeta' = 0.4$ is equivalent to $z = 8z_0$.}
\label{fig:gaussianperiodicbig_slice}
\end{figure}


\paragraph{Extrapolation capabilities} While the collocation points for the model presented in Fig.~\ref{fig:gaussianperiodicbig} were only placed for $\xi' \in [-1,1]$ and $\zeta' \in [0,1]$, the model itself can provide predictions anywhere. Thus, it is possible to evaluate the extrapolation capabilities of this model by generating predictions in $\xi' \in [-2,2]$ and $\zeta' \in [0,2]$ and comparing against the exact solution. These results are presented in Fig.~\ref{fig:gaussianperiodic_extrapol}. 

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_gaussian_periodic_extrapol/int_gauss_compare_all_calc16}
\caption{Comparison of model predictions, exact analytical solution and the error with definitions analogous to Fig.~\ref{fig:gaussianperiodicsmall}. The model used is the same as in Fig.~\ref{fig:gaussianperiodicbig}, however, $\xi' \in[-2,2]$ and $\zeta' \in [0,2]$ was now chosen, thus increasing the area analyzed by a factor of $4$. The region where collocation points were placed is encased by the dashed, gray line. }
\label{fig:gaussianperiodic_extrapol}
\end{figure}

It is obvious that the extrapolation capabilities of the model are quite limited. Notably, extrapolation along the $z$-axis is much better than along the $x$-axis. One simple explanation for this phenomenon is the nature of paraxial waves -- by design, these waves vary only slowly along the $z$-axis. Thus, the $z$-dynamics is much easier to extrapolate. In conclusion, the ability of the PINN to extrapolate is very limited. If the dynamics in the ``untrained region'' is a primitive extension of the ``trained region'', such as for $\zeta' \in [1,2]$, $\xi' \in [-1,1]$, extrapolation may be satisfactory. For other regions (especially $\xi' \in [-1, -2]$ and $\xi' \in [1,2]$), the results are in now way reliable since the high frequencies cannot be extrapolated.\par  
It has to be concluded that extrapolation for this PINN -- and presumably most PINNs -- is not a viable option. This, however, is neither particulary surprising, nor is it problematic. If one were to decide that the region were a given PINN is trained on is too small, one could either use sequence-to-sequence learning in order to model the extended region with two PINNs or one could set up a new PINN with curriculum learning by initializing the weights with the trained weights of the ``smaller'' PINN. Both of these approaches would lead to a significant time save in the training process.


\paragraph{Equivalence to Dirichlet boundary conditions for large boundaries}

If the boundary is set at a value far enough away so that the beam never comes close to it ($A(x_b, z) \approx 0$), the expectation is that Dirichlet boundary conditions should perform comparably to symmetric boundary conditions. This expectation is confirmed by numerical experiments, see Fig.~\ref{fig:gaussiandirichletsmall}. Here, the calculation for Fig.~\ref{fig:gaussianperiodicsmall} is repeated with Dirichlet boundary conditions. All other settings and hyperparameters were kept the same.

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_gaussian_dirichlet/predictions_calc21.png}
\caption{Model predictions for a PINN trained on Dirichlet boundary conditions where $\xi_b = \pm 6$ was chosen such that $|A(\xi, \zeta)|^2 \rar 0, \ \xi \rar \xi_b$. The first normalization is employed with $\xi = x / w_0$ and $\zeta = z  / z_0$.}
\label{fig:gaussiandirichletsmall}
\end{figure}

One may compare these results to Fig.~\ref{fig:gaussianperiodicsmall} both visually and quantitively, by comparing the relative $\L_2$ error. The errors are of the same magnitude, although Dirichlet boundaries perform slightly worse. This may be because $|A(\xi_b,\zeta)|^2$ is only approximately zero; in fact, at worst, it is $\num{3e-7}$ (with $u(\xi_b, \zeta) = \num{-3e-4}$ and $v(\xi_b, \zeta) = \num{4e-4}$).


\paragraph{Dirichlet boundary conditions for small boundaries}

The paraxial wave equation is strongly dependent on the bondary condition. By choosing Dirichlet conditions, the solution of the paraxial Helmholtz equation, mathematically, ceases to be \eqref{eq:gaussianwaves_2D} since back reflections are now expected at the boundaries. Such Dirichlet boundary conditions could be experimentally realized by placing a metal at the boundaries. \par 
Obviously, for regions where the beam doesn't come close to the boundary, as discussed last paragraph, virtually no back reflection is expected and the solution converges to \eqref{eq:gaussianwaves_2D}. In this section however, a PINN is set up in a way that the beam hits the boundaries. %As will be shortly seen, we obtain back reflection. \par 
This is achieved by choosing the secondly discussed normalization $A(\xi', \zeta')$ with $\xi' = \frac{x}{10w_0}$ and $\zeta' = \frac{z}{20z_0}$, \eqref{eq:gaussnorm2}, where $\xi' \in [-0.5, 0.5]$ and $\zeta' \in [0, 1]$.  \par 
The model's weights are notably not initialized with Glorot uniform but rather with the weights of the model presented in Fig.~\ref{fig:gaussianperiodicbig}, where symmetric boundary conditions where employed. This can be interpreted as curriculum learning: it is presumed that back reflections are difficult to be learned and thus, a solution without them is presented to the PINN as first guidance. Training then proceeds over $\num{10000}$ iterations ($\num{7000}$ Adam, $3000$ L-BFGS).

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_gaussian_dirichlet/predictions_calc19.png}
\caption{Model predictions for a PINN trained on Dirichlet boundary conditions with where $\xi'_b = \pm 0.5$ was chosen such that $|A(\xi'_b,\zeta)|^2 \gg 0$. The second normalization is employed with $\xi' = x / 10w_0$ and $\zeta' = z  / 20z_0$.}
\label{fig:gaussianbackreflection}
\end{figure}

It is easily seen that for these settings, one encounters significant back reflections in Fig.~\ref{fig:gaussianbackreflection}. Obviously, no analytical solution can be provided for this solution and thus, an indepth discussion of these findings will not be carried out.


\paragraph{Oddities: fast convergence on an ill-defined problem}

Studying the literature, one obtains the impression that PINNs are highly volatile with respect to the boundary conditions. For many problems, this may be the case. The paraxial Helmholtz equations with a Gaussian as initial condition exhibits, peculiarily, the opposite property: the PINN excells most in approximating $A(x,z)$ when provided with no boundary condition at all.\par 
As proof, consider Fig.~\ref{fig:gaussianoddity} and compare the results to symmetric boundary conditions presented in Fig.~\ref{fig:gaussianperiodicbig}, where, besides the boundary condition, the PINNs are identical. The relative $\L_2$ errors obtained without boundary condition are smaller by a factor of between $1.5$ to $2$. Note also that, while not shown here, the PINN in Fig~\ref{fig:gaussianoddity} has greater extrapolation capabilities than the PINN of Fig.~\ref{fig:gaussianperiodic_extrapol}. Furthermore, this phenomenon does not only occor for $\xi' \in [-1,1]$, where $A(\xi' = \pm 1, \zeta')$ is rather small -- it also occurs for $\xi' \in [-0.5,0.5]$ where for the same PINN with Dirichlet boundary conditions, back reflections were observed in Fig.~\ref{fig:gaussianbackreflection}, thus proving that the boundary condition has a significant impact on the solution.

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_gaussian_nobound/int_gauss_compare_all_calc15}
\caption{Comparison of model predictions, exact analytical solution \eqref{eq:gaussianwaves_2D} (for boundary condition $A(x,z) \rar 0, \ x\rar\infty$) and the error with definitions analogous to Fig.~\ref{fig:gaussianperiodicsmall}. No boundary condition was supplied to the neural network. Nonetheless, convergence to a reasonable solution was achieved.}
\label{fig:gaussianoddity}
\end{figure}

The paraxial Helmholtz equation without boundary conditions is, technically, an ill-defined problem. Therefore, one would expect a PINN to have to choose between infinitively many solutions, thus either converging to an arbitrary solution or, in fact, no solution whatsoever. One intuitive explanation -- albeit seriously lacking in rigour -- is that, empirically, PINNs take the ``easiest'' path (see the discussion on the beam width, Fig.~\ref{fig:beamwidthgaussian} where this hypothesis was introduced). This, for example, is reflected by the fact that PINNs generally struggle approximating solutions of high frequency. One could imagine that the boundary condition $A(x,z) \rar 0, \ x\rar 0$ is, for this PDE, one of the ``easiest''. \par
Still, mathematically, symmetric boundary conditions for this problem yield the same result as $A(x,z) \rar 0, \ x\rar \infty$. Why then would limiting the solution space lead to worse performance? %One possible explanation lies in the derivatives which for symmetric boundary conditions are calculated at the boundary. Perhaps floating point precision (\texttt{float32} being employed) is a limiting factor thereof.\par 
Overall, further research on this phenomenon will be necessary:
\begin{itemize}
\item Can this behavious be observed for other differential equations?
\item What is the impact of the initial condition? Can a particular solution be selected by different choices of initial conditions?
\item Can a particular solution be selected by providing a small amount of experimental data?
\end{itemize}


\subsection{Summary and discussion}

In this section, several PINNs for the Gaussian beam propagation in free space were presented and analyzed.\par 
Both symmetric and Dirichlet boundary conditions were employed for cases where the boundary extend $\xi_b$ was chosen such that $A(\xi_b, \zeta) \approx 0$. In both cases, the same solution was found by the PINNs (Fig.~\ref{fig:gaussianperiodicsmall} for symmetric boundary, Fig.~\ref{fig:gaussiandirichletsmall} for Dirichlet boundary). While symmetric boundaries perform slightly better, the solutions only differ by a factor of $1.5$ to $2$ in their relative $\L_2$ errors. The errors are in the regime of $10^{-2}$.\par 
Compared with literature results for other 2D problems \cite{raissiPhysicsinformedNeuralNetworks2019}, these errors are not ideal. %The reasons for this could potentially be manifold. Perhaps, the PINN converged to a local minimum with significantly higher cost $J$ than the global minimum. This, however, is highly speculative. \textbf{actually Ian Goodfellow describes a method to exclude local minima as the problem, I should look at that} \par 
A lower error could presumably be achieved with more layers, more nodes or more collocation points. Similarily, other advanced methods such as learning rate annealing or locally adaptive activation functions could be employed. It is up to the experimentalist to determine how big the error for a particular problem may be in order for the PINN to still be instructive. The PINNs presented in this Chapter have not been optimized to the fullest; it was rather aimed at striking a good compromise between training speed and relative $\L_2$ error.\par 
Further analysis on symmetric boundaries was conducted for a PINN with another normalization ($\xi' = x / 10w_0$, $\zeta' = z / 20z_0$) which corresponded to ``zooming out'' relative to the normalization chosen before ($\xi = x / w_0$, $\zeta = z / z_0$). Thus, there were more features to approximate and, notably, more high frequency components present in the solution. However, the PINN managed this task without complications and achieved a relative $\L_2$ error with similar magnitude to the first normalization. For this PINN, it was also studied how well it was able to extrapolate outside the region where collocation points were placed. It was found that in the $z$-direction, extrapolation was possible since paraxial waves, by design, do not vary significantly along $z$, the optical axis. However, along the $x$-axis, the extrapolation capabilities of the model are crude.\par 
For the secondly discussed normaliation ($\xi' = x / 10w_0$, $\zeta' = z / 20z_0$) and $\xi' \in [-0.5, 0.5]$ with Dirichlet boundary conditions, back reflections were both expected and found. This demonstrates how the solution of the paraxial Helmholtz equation is strongly dependent on the boundary conditions. All the more surprising is to observe that the best performing PINN approximating \eqref{eq:gaussianwaves_2D} is, in fact, the one without any boundary conditions. %Possible reasons for this behaviour are more akin to speculation than sound scientific arguments. 
More research on this phenomenon will be necessary.\par 
This marks the end of the discussion of Gaussian beam propagation in free space. The Gaussian beam itself will however return when, among others, a PINN for guiding of a Gaussian beam in a quadratic-index medium is presented next section.




%\begin{figure}[H]
%\centering
%\includegraphics{graphics/chapter4_periodic/beam_width_gaussian.pdf}
%\caption{Beam width $w(z)$ of the reference solution as well as trained model.}
%\label{fig:beamwidthgaussian}
%\end{figure}


%\begin{figure}[H]
%\centering
%\includegraphics{graphics/chapter4_periodic/int_gauss_compare.png}
%\caption{Comparison of the intensity profiles for the Gaussian beam propagation. a) Exact solution $|A_\mathrm{ex}(x, z)|^2$. b) Predicted solution $|A_\mathrm{pred}(x, z)|^2$ of the trained model. c) Error $\Delta |A|^2$ of the neural network where $\Delta |A|^2 = \left| |A_\mathrm{ex}(x, z)|^2 - |A_\mathrm{pred}(x, z)|^2 \right| $.}
%\label{fig:gaussiancomparison}
%\end{figure}


\newpage

\section{The parabolic wave guide} \label{sec:pinnwaveguide}

In the following, PINNs for the parabolic wave guide will be presented. Analogously to the Gaussian beam propagation, we will first present the real and imaginary part of the PDE, discuss boundary conditions, normalizations and chosen hyperparameters. Then, three PINNs will be presented, corresponding to
\begin{enumerate}
\item guiding a Gaussian beam,
\item guiding a simple superposition of the first two Hermite-Gaussian modes and
\item guiding a randomly determined superposition of Hermite-Gaussian modes.
\end{enumerate}

\subsection{Setting up the problem}

\paragraph{Real and imaginary components} As derived in Chapter~\ref{subsec:parabolicwaveguide}, the propagation of paraxial waves in a quadratic-index medium is described by an extension of the paraxial wave equation, 
\eqref{eq:paraxialwaveeq_1d}, which is again provided here for readabilities sake.
\begin{align} \label{eq:paraxialwaveeq_1d_ch4}
\ppxz A(x,z) + 2 \i n_0 k_0 \ppz A(x,z) - 2 a n_0 k_0^2 x^2 \, A(x,z) = 0.
\end{align}
Similarily to the Gaussian beam propagation in free space which was described by a PINN last section, the differential equation will be separated in a real and imaginary part. Let thus
\begin{equation}
A(x,z) \coloneqq u(x,z) + \i v(x,z).
\end{equation}
Substituting in \eqref{eq:paraxialwaveeq_1d_ch4} gives
\begin{multline}
\frac{\partial^2 }{\partial x^2} u(x,z) - 2n_0 k_0 \partial_z v(x,z) - 2an_0 k_0^2 x^2 \, u(x,z) \\ + \i\cdot \left( \frac{\partial^2 }{\partial x^2} v(x,z) + 2n_0 k_0 \partial_z u(x,z) - 2an_0 k_0^2 x^2 \, v(x,z) \right) = 0,
\end{multline}
yielding
\begin{align}
0 &= \frac{\partial^2 }{\partial x^2} u(x,z) - 2n_0 k_0 \partial_z v(x,z) - 2an_0 k_0^2 x^2 \, u(x,z) \\
0 &= \frac{\partial^2 }{\partial x^2} v(x,z) + 2n_0 k_0 \partial_z u(x,z) - 2an_0 k_0^2 x^2 \, v(x,z).
\end{align}
The stationary solutions of \eqref{eq:paraxialwaveeq_1d_ch4} are the Hermite-Gaussian modes, \eqref{eq:solutionwaveguide}, again provided here.
\begin{equation} \label{eq:solutionwaveguide_ch4}
A(x,z) = \sum_n c_n g_n(x) \e^{-\frac{\i M_n}{2n_0 k_0} z}
\end{equation}
Since $A(x,z)$ is complex-valued, the PINN optimizes the real and imaginary part, $A(x,z) = u(x,z) + \i v(x,z)$ separately. Again, $u(x,z)$ and $v(x,z)$ and thus the initial condition are not given explicitly but rather obtained numerically.

\paragraph{Boundary conditions} Dirichlet boundary conditions
\begin{equation}
A(-x_b, z) = A(x_b, z) = 0,
\end{equation}
with $x = x_b$ the extent of the boundary, will be employed.


\paragraph{Normalization} It is most convenient to normalize the equation with respect to the wavelength. With $\xi = \frac{x}{2\lambda}$, $\zeta = \frac{z}{5\lambda}$ and $\alpha = a \lambda^2$, the PDE transforms to
\begin{equation}
\ppxiz A(\xi,\zeta) +  \frac{16}{5} \pi \i n_0 \ppze A(\xi,\zeta) - 64 \pi^2 \alpha n_0  \xi^2 \, A(\xi,\zeta) = 0.
\end{equation}
The stationary solutions are now given by
\begin{equation}
g_n(\xi) =  \frac{1}{\sqrt{2^n n!}} \left( 8 \sqrt{\alpha n_0} \right)^{1/4} \e^{-4 \pi \xi^2 \sqrt{\alpha n_0}} H_n\left(\sqrt{8\pi} (\alpha n_0)^{1/4} \xi \right)
\end{equation}
with eigenvalues
\begin{equation}
M_n = 16\pi \sqrt{\alpha n_0} \left(n+\frac{1}{2}\right) 
\end{equation}
and thus the full time-dependent solution
\begin{align}
A(\xi, \zeta) = \sum_n c_n g_n(\xi) \e^{-5 \i \sqrt{\alpha / n_0} \left(n + \frac{1}{2}\right) \zeta}.
\end{align}


\subsection{Training results}


\begin{equation}
c^{(1)} = \begin{pmatrix}
1 \\ 0 \\ \vdots
\end{pmatrix}, \ \ c^{(2)} = \begin{pmatrix}
1 + \i \\ 1 + \i \\ 0 \\ \vdots
\end{pmatrix}, \ \ c^{(3)} = \begin{pmatrix}
0 \\ 1 - 0.75\i \\ 0.75 + 0.5\i \\ 0.5 - 0.25\i \\ 0.25 + 0.1\i \\ 0 \\ \vdots
\end{pmatrix}
\end{equation}


\paragraph{Guiding a Gaussian beam} \hspace{1pt} \par 


\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_waveguide/waveguide_compare_all_calc8}
\caption{Comparison of the model predictions (index $\mathrm{pred}$, \textit{left column}) with the exact analytical solution (index $\mathrm{ex}$, \textit{middle column}) and the errors $\Delta \beta = \left| \beta_\mathrm{pred} - \beta_\mathrm{ex}\right|$, $\beta \in \{u, v, |A|^2\}$ (\textit{right column}). The first row shows this analysis for $u(x,z)$, the second row for $v(x,z)$ and the third row for $|A(x,z)|^2$, where $A(x,z) = u(x,z) + \i v(x,z)$ is the complex amplitude \eqref{eq:paraxialwaveeq_1d_ch4}. The $x$-axis is normalized by $2\lambda$ and the $z$-axis by $5\lambda$.}
\label{fig:waveguide_gaussian}
\end{figure}



\paragraph{Guiding a simple superposition of two TEM modes}

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_waveguide/waveguide_compare_all_calc7}
\caption{Comparison of the model predictions (index $\mathrm{pred}$, \textit{left column}) with the exact analytical solution (index $\mathrm{ex}$, \textit{middle column}) and the errors $\Delta \beta = \left| \beta_\mathrm{pred} - \beta_\mathrm{ex}\right|$, $\beta \in \{u, v, |A|^2\}$ (\textit{right column}). The first row shows this analysis for $u(x,z)$, the second row for $v(x,z)$ and the third row for $|A(x,z)|^2$, where $A(x,z) = u(x,z) + \i v(x,z)$ is the complex amplitude \eqref{eq:paraxialwaveeq_1d_ch4}. The $x$-axis is normalized by $2\lambda$ and the $z$-axis by $5\lambda$.}
\label{fig:waveguide_simplesuperpos}
\end{figure}


%\begin{figure}[H]
%\centering
%\includegraphics{graphics/chapter4_waveguide/marker.pdf}
%\caption{Collocation points used in model training where yellow are $200$ points on the boundary, red are $200$ points for the initial solution and blue are $10000$ points at which the differential equation is enforced. All points are generated by random uniform sampling.}
%\label{fig:markerQMHO}
%\end{figure}

%\begin{figure}[H]
%\centering
%\includegraphics{graphics/chapter4_waveguide/loss_epoch.pdf}
%\caption{Loss dynamic during the training process.}
%\label{fig:lossQMHO}
%\end{figure}

%\begin{figure}[H]
%\centering
%\includegraphics{graphics/chapter4_waveguide/int_compare_all.png}
%\caption{Comparison of the predicted wavefunction with the exact solution. \\ $1^\text{st}$ column: solutions predicted by the PINN. $2^\text{nd}$ column: reference solution, \eqref{eq:solutionwaveguide}. $3^\text{rd}$ column: absolute error, given by $\left| \xi_\mathrm{ref} - \xi_\mathrm{pred} \right|$ where $\xi \in \left\{ u(x,z), \: v(x,z), \: |A(x,z)|^2 \right\}$. \\ $1^\text{st}$ row: analysis of $u(x,z)$. $2^\text{nd}$ row: analysis of $v(x,z)$. $3^\text{rd}$ row: analysis of $|A(x,z)|^2$.}
%\label{fig:intcompareQMHO}
%\end{figure}

%\begin{figure}[H]
%\centering
%\includegraphics{graphics/chapter4_waveguide/initialsolution.pdf}
%\caption{Initial solution $A(x, 0) = u(x, 0) + \i v(x, 0)$ as predicted by the PINN (dots) compared with the reference solution (solid).}
%\label{fig:initialsolQMHO}
%\end{figure}