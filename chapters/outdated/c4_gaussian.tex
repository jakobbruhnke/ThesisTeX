\chapter[Solving the paraxial Helmholtz equation]{Solving the paraxial Helmholtz equation} \label{ch:pinngaussianwave}

By now, we have reviewed Gaussian beam propagation and the parabolic wave guide in Chapter~\ref{ch:optics}, delved into the basic theory and intuition behind feed-forward neural networks in Chapter~\ref{ch:nns} and examined PINNs as an extension of these networks in Chapter~\ref{ch:pinns}. With the underlying theory being adequately adressed, this Chapter will build upon these foundations in order to present PINNs solving the paraxial Helmholtz equation with an initial Gaussian distribution in free space. Various aspects will be investigated:
\begin{itemize}
\item What is the impact of the boundary condition? 
\begin{itemize}
\item Can reflections at the boundary be observed for Dirichlet boundary conditions
\end{itemize}
\item How does training differ based on how many Rayleigh lengths are studied
\item What are the extrapolation capabilities, i.e. does the model produce physically reasonable results outside the region where collocation points are placed?
\end{itemize}

\section{Setting up the two-dimensional problem}

One important particular solution of the paraxial Helmholtz equation is the Gaussian beam $A(\br)$, \eqref{eq:gaussianwaves}. Naturally, one can set up a PINN to solve this three-dimensional problem and indeed, this has been tried so some success by the author of this thesis. However, more dimensions are detrimental to the speed of convergence. On high-performance clusters, the training speed of this particular problem in 3D will presumably still be reasonable (in the regime of several hours). However, since the PINN here presented is to be understood as a ``toy problem'' set up in order to provide a suitable introduction to the paraxial wave equation of a quadratic index medium (which will be treated in 2D, $x$- and $z$-dimensions only), it is sufficient for the scope of this thesis to solve the paraxial Helmholtz equation in free space in two dimensions, $x$ and $z$, only.

\paragraph{Dimensionality reduction of the paraxial wave equation} The paraxial wave equation is given by \eqref{eq:paraxialhelmholtz_vac}. In 2D we obtain
\begin{equation} \label{eq:paraxialhelmholtz_vac_2d_ch4}
\ppxz A(x,z) + 2\i k \ppz A(x,z) = 0.
\end{equation} 
Its most well-known solution is the Gaussian wave, \eqref{eq:gaussianwaves}, which in two dimensions is given by
\begin{equation} \label{eq:gaussianwaves_2D}
A(x,z) = \frac{A_0}{\sqrt{q(z)}} \exp\left[\i k\frac{x^2}{2q(z)}\right],
\end{equation}
$q(z) = z - \i z_0$, simply obtained by factorizing $A(x,y,z) = A(x,z) A(y,z)$. Please note that \eqref{eq:gaussianwaves_2D} is a solution for the boundary condition $A(\pm x,z) \rar 0, \ x\rar \infty$. Numerically, this solution can be approximated with Dirichlet conditions given that $A(\pm x_b, z) \approx 0$ where $x_b$ is the boundary position.

\paragraph{Real and imaginary components} Both the solution as well as the differential equation itself are complex. While \texttt{tensorflow} allows for complex datatypes, this is not mirrored by other machine learning libraries. In most publications on PINNs, only real datatypes for neural networks are utilized and this thesis is no different in this respect.\par  %While there are current developments to enable better compatibility of complex datatypes with machine learning frameworks such as \texttt{tensorflow} \textbf{SOURCE - arXiv:2009.08340v2}, in this work, for ease of compatibility with other libraries, only real datatypes will be utilized.\par 
Thus, the complex-valued PDE has to be split up into two coupled differential equations for the real and imaginary part. Let
\begin{equation}
A(x,z) \coloneqq u(x,z) + \i v(x,z).
\end{equation}
Substitution into \eqref{eq:paraxialhelmholtz_vac_2d_ch4} yields two coupled differential equations forming the basis for the PINN,
\begin{align}
\frac{\partial^2 }{\partial x^2} u(x,z) - 2k\partial_z v(x,z) &= 0 \\
\frac{\partial^2 }{\partial x^2} v(x,z) + 2k\partial_z u(x,z) &= 0
\end{align}
One can easily give explicit expressions for $u(x,z)$ and $v(x,z)$ by splitting \eqref{eq:gaussianwaves_2D} into its real and imaginary part. However, this is hardly necessary for most cases. If one desires to plot $u$ and $v$ in any programming language capable of dealing with complex datatypes (such as \texttt{Python} and its array library \texttt{numpy}), it is most natural to create a complex-valued function \texttt{A(x,z)} and then calculate \texttt{u = real(A(x,z))} and \texttt{v = imag(A(x,z))}.

\paragraph{Boundary conditions} The position of the boundary, $x = x_b$, will be varied in the upcoming studies. We will examine both Dirichlet boundary conditions
\begin{align}
A(\pm x_b, z) = 0
\end{align}
as well as periodic boundary conditions
\begin{align}
A(-x_b, z) &= A(x_b, z) .
\ppx A(x,z) \biggl|_{-x_b} &= -\ppx A(x,z) \biggl|_{x_b}
\end{align}
and, lastly, also investigate a PINN without specifying any boundary conditions at all.\par 
Now, it is well-known that the boundary condition has dramatic impact on the solution. The solution only resembles the freely propagating Gaussian wave, \eqref{eq:gaussianwaves_2D}, when the beam does not reach the boundaries, i.e. $|A(\pm x_b,z)| \approx 0$ for boundary positions $\pm x_b$. Thus, it is only valid to compare the model predictions to \eqref{eq:gaussianwaves_2D} if this prerequisite is fulfilled. In cases where $|A(\pm x_b,z)| \neq 0$, interference on the boundaries induced either by reflections (Dirichlet) or an incoming beam due to periodicity. Without numerical simulations, these PINNs can then only be evaluated qualitatively.




\paragraph{Normalization} Using actual physical units (such as $\si{\nano\meter}$ for the wavelength) is impractical for computational tasks due to floating point errors and the high computational cost attributed to increased floating point precision. Thus, the solution and PDE have to be normalized. For the Gaussian beam, it is natural to normalize $x$ with the beam width $w_0$ and $z$ with the Rayleigh length $z_0$. For these normalized coordinates $\xi = x / w_0$ and $\zeta = z / z_0$ (in subsequent text also referred to as the ``\textit{first normalization}''), the PDE can be recast to
\begin{align}
\ppxiz A(\xi, \zeta) + 4\i \ppze A(\xi, \zeta) = 0
\end{align}
solved by the normalized Gaussian beam
\begin{align} \label{eq:gaussnorm1}
A(\xi,\zeta) = \frac{A_0}{\sqrt{z_0 (\zeta - \i)}} \exp\left[\i \frac{\xi^2}{\zeta - \i}\right].
\end{align}
Similarily, the boundary value is now denoted as $\xi_b$. This normalization will be used first to show the equivalence of Dirichlet and symmetric boundary conditions in cases where $A(\xi, \zeta) \rar 0, \ \xi \rar \xi_b$. As will be justified, this is the case for $\xi_b = \pm 6$ and $\zeta \in [0,2]$. \par 
Later on, another normalization (in subsequent text also referred to as the ``\textit{second normalization}'') will be used, where $\xi' = \frac{x}{10w_0}$ and $\zeta' = \frac{z}{20z_0}$. The PDE is thus recast to
\begin{align} \label{eq:gaussnorm2pde}
\frac{1}{5} \ppxisz A(\xi', \zeta') + 4\i \ppzes A(\xi', \zeta') = 0
\end{align}
with solution
\begin{align} \label{eq:gaussnorm2}
A(\xi',\zeta') = \frac{A_0}{\sqrt{z_0 (20\zeta' - \i)}} \exp\left[\i \frac{(10\xi')^2}{20\zeta' - \i}\right].
\end{align}
For this normalization, the boundary value $\xi'_b$ will be chosen as $\xi'_b = \pm 1$ (periodic boundary) and $\xi'_b = \pm 0.5$ (Dirichlet boundary), where $\zeta \in [0,1]$. 
Physically speaking, with the second normalization, we thus consider a beam which has a stronger radius of curvature; the beam gets wider compared to the first normalization. Therefore, we expect that a larger numerical effort is needed to solve \eqref{eq:gaussnorm2pde}. \par 
%Note that this second normalization is equivalent to ``zooming out'' of the first normalization. Thus, $A(\xi', \zeta')$ for the same extent of the boundaries, is more complicated and has more high-frequeny components than $A(\xi, \zeta)$. \par 
For both normalizations, we choose
\begin{equation}
\frac{A_0}{\sqrt{z_0}} = 1
\end{equation}
for simplicity and ease of training. As initial condition, $A(\xi,\zeta=0)$ or analogously $A(\xi', \zeta'=0)$ will be provided to the PINN.


\paragraph{Hyperparameters and other settings} In Tab.~\ref{tab:hyperparameter_gaussian}, the hyperparameters which remain constant throughout this section are provided, together with other settings.

\begin{table}[H]
    \centering
    \caption{Listing of the hyperparameters and chosen settings which all PINNs presented in this Chapter have in common.}
    \label{tab:hyperparameter_gaussian}
    \rowcolors{1}{tableblue}{White}
    \begin{tabularx}{.95\textwidth}{lcX}
    \arrayrulecolor{darktableblue} \hline 
    Hyperparameter & Symbol & Used for upcoming calculations \\ \hline
    Number of layers & $L$ & $\num{5}$ \\
    Nodes per layer & $n_l$ & $\num{32}$ for all layers \\
    Adam learning rate & $\alpha$ & $\num{1e-3}$  \\
    Collocation points (PDE) & $m_f$ & with boundary cond.: $\num{10000}$, \newline without boundary cond.: comparison of $\num{10000}$ and $\num{15000}$  \\
    Collocation points (Bound.) & $m_b$ & $\num{200}$  \\
    Collocation points (Init.) & $m_\mathrm{init}$ & $\num{200}$  \\
    Sampling of colloc. points & -- & Random numbers (uniform distribution) \\
    Weight factor (PDE) & $w_f$ & First normalization, $A(\xi,\zeta)$: $\num{5e-4}$, \newline second normalization, $A(\xi', \zeta')$: $\num{1e-3}$  \\
    Weight factor (boundary) & $w_b$ & Symmetric boundary: $0.5$, \newline Dirichlet boundary: $1$ \\
    Weight factor (init.) & $w_\mathrm{init}$ & $\num{1}$  \\
    Optimizer & -- & In succession: Adam and L-BFGS  \\
    Activation function & $\mathrm{act}$ & $\tanh$ \\
    Loss function & -- & Mean squared error ($\mathrm{MSE}$) \\ \hline
    \end{tabularx}
\end{table}

\section{Training results}

\subsection{Periodic boundary conditions} \label{subsec:periodicbound} %Solving the paraxial wave equation with periodic boundary conditions}

\paragraph{First normalization} First, $A(\xi, \zeta)$ (first normalization, as in \eqref{eq:gaussnorm1}) was approximated by a PINN while enforcing symmetric boundary conditions with $\xi \in [-6, 6]$ and $\zeta \in [0, 2]$. Glorot initialization was used. Notably, the floating point precision was set to \texttt{float64} (double precision) since the gradients encountered were too small for L-BFGS to handle with \texttt{float32} (single precision). \par  
Training was completed after $\num{11000}$ iterations of whom $\num{8000}$ were performed with the Adam optimizer and the last $\num{2000}$ with L-BFGS. Trained on a small GPU (NVIDIA GeForce 940MX), training took approximately $\SI{120}{\min}$. Of those, Adam optimization took $\SI{80}{\min}$ and L-BFGS $\SI{40}{\min}$, demonstrating the increased computational cost of second-order methods.\par 
One may wonder if it wouldn't be beneficial to always train with double precision. Concerning convergence, higher precision has indeed no disadvantages. However, computational cost increases dramatically -- for single point precision with $\num{12000}$ Adam and $\num{3000}$ L-BFGS iterations, training with the same hardware takes $\SI{40}{\min}$ ($\SI{17}{\min}$ Adam, $\SI{23}{\min}$ L-BFGS). Thus double precision more than doubles training time. For many PINNs, single precision is more than enough and it has to be decided on a case to case basis if double precision is actually worth the computational effort.

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_gaussian_periodic2/int_gauss_compare_all_calc24}
\caption{Comparison of the model predictions (index $\mathrm{pred}$, \textit{left column}) with the exact analytical solution (index $\mathrm{ex}$, \textit{middle column}) and the errors $\Delta \beta = \left| \beta_\mathrm{pred} - \beta_\mathrm{ex}\right|$, $\beta \in \{u, v, |A|^2\}$ (\textit{right column}). The first row shows this analysis for $u(x,z)$, the second row for $v(x,z)$ and the third row for $|A(x,z)|^2$, where $A(x,z) = u(x,z) + \i v(x,z)$ is the complex amplitude \eqref{eq:gaussianwaves_2D}.}
\label{fig:gaussianperiodicsmall}
\end{figure}

The results are depicted in Fig.~\ref{fig:gaussianperiodicsmall}. Visually, comparing the left and middle columns (predictions and exact solution respectively), good agreement can be observed. It can be concluded that the PINN architecture is well suited for the task. Notably though, the regions of high error (Fig.~\ref{fig:gaussianperiodicsmall}, c) and f)) seem systematic in the sense that they appear as periodic stripes along the $z$ axis, where in Fig.~\ref{fig:gaussianperiodicsmall}, a) and d), the fringes of the beam are located.\par 
The relative $\L_2$ errors for $u(x,z)$ and $v(x,z)$ are of magnitude $\num{1e-2}$. Meanwhile, the intensity $|A|^2$ -- which, in any experiment, would be the physical entity observable -- has an error of only $\num{6.7e-3}$ since the errors of $u$ and $v$ cancel out.\par 
While acceptable for many purposes, the errors of $u$ and $v$ are not ideal. In the literature, errors between $\num{1e-4}$ and $\num{1e-3}$ are no rarity \cite{raissiPhysicsinformedNeuralNetworks2019}. It is likely that performance can be greatly improved by systematically optimizing for parameters such as the layer size $L$, number of nodes per layer $n_l$, collocation points $N_{f/b/\mathrm{init}}$ or weight factors $w_{f/b/\mathrm{init}}$. \par
Significantly, the error is not particularily large at the boundaries. High errors at the boundaries are commonly observed for PINNs. They originate from \textit{vanishing gradient pathologies} \cite{wangUnderstandingMitigatingGradient2021, markidisOldNewCan2021} and can be adressed by locally adaptive activation functions \cite{jagtapLocallyAdaptiveActivation2020}. \par


\paragraph{Beam width} Given the intensity $|A(x,z)|^2$ predicted by the model, the beam width \eqref{eq:beamwidth} can be obtained by fitting a Gaussian onto the transverse beam profile at each $z$ value. For the normal distribution $f(x)$, parametric for $z$,
\begin{equation}
f(x;z) = \frac{A}{\sqrt{2\pi} \sigma(z)} \e^{-\frac{x^2}{2\sigma^2(z)}},
\end{equation}
the beam width $w(z)$ can easily be obtained:
\begin{equation}
w(z) = 2\sigma(z).
\end{equation}
The resulting beam width is shown in Fig.~\ref{fig:gaussianperiodicsmall_beamwidth} (red, dashed). Alongside, the exact solution \eqref{eq:beamwidth} (blue, solid) and the model prediction for single precision, i.e. float32 (green, dotted) is given. It can be concluded that the PINN with double precision is qualitatively and quantitively able to predict the beam dynamics. With only single precision, the predicted beam width is for higher $z$ values slightly smaller than the exact value.


\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_gaussian_periodic2/beam_width_gaussian_calc24}
\caption{Comparison of the exact beam width $w(z)$, \eqref{eq:beamwidth}, with the model prediction.}
\label{fig:gaussianperiodicsmall_beamwidth}
\end{figure}




\paragraph{Second normalization} While the previous PINN was successful, it has to be emphasized that the function to be approximated was of low complexity. A larger challenge is the approximation of $A(\xi', \zeta')$ (second normalization, as in \eqref{eq:gaussnorm2}) where $\xi' \in [1, 1]$ and $\zeta' \in [0, 1]$. Choosing these parameters corresponds to ``zooming out'' of the region approximated in Fig.~\ref{fig:gaussianperiodicsmall}.\par 
Training was completed after $\num{12000}$ iterations ($\num{8000}$ Adam, $4000$ L-BFGS). Again, double precision (\texttt{float64}) was employed.

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_gaussian_periodic2/int_gauss_compare_all_calc25}
\caption{Comparison of model predictions, exact analytical solution and the error with definitions analogous to Fig.~\ref{fig:gaussianperiodicsmall}. The secondly discussed normalization with $\xi' = x / 10w_0$ and $\zeta' = z / 20z_0$ is employed with $\xi' \in [-1,1]$ and $\zeta' \in [0,1]$.}
\label{fig:gaussianperiodicbig}
\end{figure}

The results for the second normalization are shown in Fig.~\ref{fig:gaussianperiodicbig}. Good agreement is achieved. The errors are slightly higher but, judging by the increased number of fringes, this was to be expected to some degree. The same periodicity of regions of high error can be observed at the fringes. This can be better visualized by taking a transverse slice of $A(\xi', \zeta') = u(\xi', \zeta') + \i v(\xi', \zeta')$ at $\zeta' = 0.3$, corresponding to $z = 6z_0$. These transverse beam profiles are depicted in Fig.~\ref{fig:gaussianperiodicbig_slice}. \par 
We see that the PINN struggles to reach the maxima and minima of the side-oscillations most close to the boundary. To some degree, it is to be expected that these features are the most difficult to approximate -- PINNs generally tend to struggle with multi-scale features (although the difference in scale at the fringes is still rather mild) and also, as discussed earlier, the boundaries can be troublesome due to vanishing gradients.

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_gaussian_periodic2/mid_solution_calc25.pdf}
\caption{Transverse beam profiles for $u(\xi', \zeta' = 0.3)$ and $v(\xi', \zeta' = 0.3)$, taken from Fig.~\ref{fig:gaussianperiodicbig}, where $\zeta' = 0.3$ is equivalent to $z = 6z_0$.}
\label{fig:gaussianperiodicbig_slice}
\end{figure}



\paragraph{Observations on the training process}

During training conducted for PINNs presented in this chapter, this author has often encountered problems of PINNs predicting slightly collimating beams. In general, the training process can be subdidived into three subsequent but non-distinct phases:
\begin{enumerate}
\item Prediction of a collimated beam (first approx. $\num{1000}$ iterations) where the initial condition is fulfilled first
\item Slow broadening of the beam (until approx. $\num{6000}$ iterations)
\item Appearance of fringes in $u$ and $v$ (slowly after approx. $\num{6000}$ iterations with Adam, then faster as soon as L-BFGS is employed), thus further leading to further broading of the beam
\end{enumerate}
As visual reference, see Fig.~\ref{fig:gaussianperiodicbig_progress}, where a collimated beam is predicted in panel \textit{a)} after $\num{500}$ iterations, a slow broading occurs by panel \textit{b)} after $\num{1500}$ iterations, a much more broadened beam is achieved due to better description of the fringes in panel \textit{c)} after $\num{8000}$ iterations and the final training result after optimization with L-BFGS is depicted in panel \textit{d)} after $\num{12000}$ iterations. Note how the beam width converges asymptotically.\par 
These findings, in general, correspond well to comments made by Markidis stating that PINNs learn low-frequency\footnote{Please note that ``frequency'' in this case does not refer to the optical frequency of light but rather how much the solution changes on a small region.} components of the solution first \cite{markidisOldNewCan2021}.

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_gaussian_periodic2/progress2_calc25.png}
\caption{Training process of the gaussian beam intensity $|A(x,z)|^2$ for the PINN presented in Fig.~\ref{fig:gaussianperiodicbig} at four different iterations ($500$, $1500$, $8000$ and $\num{12000}$) compared to the exact solution (panel \textit{e)}). The panels correspond to the training dynamics listed above: prediction of collimation, slow broadening and then further broadening due to appearance of fringes in $u$ and $v$. A logarithmic colormap is chosen to emphasize the changes between iterations.}
\label{fig:gaussianperiodicbig_progress}
\end{figure}


\subsection{Dirichlet boundary conditions} \label{subsec:dirichletbound}

%\paragraph{Equivalence to Dirichlet boundary conditions for large boundaries}

If the boundary is set at a value $x_b$ far enough away so that the beam never comes close to it ($A(x_b, z) \approx 0$), the expectation is that Dirichlet boundary conditions should yield results equivalent to \eqref{eq:gaussianwaves_2D}. This expectation is confirmed by numerical experiments, see Fig.~\ref{fig:gaussiandirichletsmall}. Here, the calculation for Fig.~\ref{fig:gaussianperiodicsmall} is repeated with Dirichlet boundary conditions. Except that single precision (\texttt{float32}) was employed, all other settings and hyperparameters were kept the same. Training was performed with $\num{12000}$ Adam and $\num{3000}$ L-BFGS iterations.

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_gaussian_dirichlet/predictions_calc21.png}
\caption{Model predictions for a PINN trained on Dirichlet boundary conditions where $\xi_b = \pm 6$ was chosen such that $|A(\xi, \zeta)|^2 \rar 0, \ \xi \rar \xi_b$. The first normalization is employed with $\xi = x / w_0$ and $\zeta = z  / z_0$.}
\label{fig:gaussiandirichletsmall}
\end{figure}

One may compare these results to Fig.~\ref{fig:gaussianperiodicsmall} both visually and quantitively, by comparing the relative $\L_2$ error. Visually, agreement is excellent. Quantitively, Dirichlet boundaries perform slightly worse. This may be because $|A(\xi_b,\zeta)|^2$ as given by \eqref{eq:gaussnorm1} is only approximately zero at the boundaries; in fact, at worst, it is $\num{3e-7}$ (with $u(\xi_b, \zeta) = \num{-3e-4}$ and $v(\xi_b, \zeta) = \num{4e-4}$). In part, it may also be due to using single point precision, although, in contrast to Section~\ref{subsec:periodicbound}, L-BFGS did in fact properly converge with single precision.\footnote{``Properly converge'' is, in this case, not a well-defined expression but rather based on experience: if L-BFGS for this PDE converges within less than $\num{1000}$ iterations, it is very likely that floating point precision is a limiting factor.}


\paragraph{Reflections at the boundary}

The paraxial wave equation is strongly dependent on the bondary condition. By choosing Dirichlet conditions, the solution of the paraxial Helmholtz equation, mathematically, ceases to be \eqref{eq:gaussianwaves_2D} since reflections are now expected at the boundaries. Such Dirichlet boundary conditions could be experimentally realized by placing a metal at the boundaries. \par 
Obviously, for regions where the beam doesn't come close to the boundary, as discussed last paragraph, virtually no reflection is expected and the solution converges to \eqref{eq:gaussianwaves_2D}. In this section however, a PINN is set up in a way that the beam hits the boundaries. %As will be shortly seen, we obtain back reflection. \par 
This is achieved by choosing the secondly discussed normalization $A(\xi', \zeta')$ with $\xi' = \frac{x}{10w_0}$ and $\zeta' = \frac{z}{20z_0}$, \eqref{eq:gaussnorm2}, where $\xi' \in [-0.5, 0.5]$ and $\zeta' \in [0, 1]$.  \par 
The model's weights are notably not initialized with Glorot uniform but rather with the weights of the model presented in Fig.~\ref{fig:gaussianperiodicbig}, where periodic boundary conditions where employed. As discussed in Chapter~\ref{subsec:convergence}, this approach can be categorized as curriculum learning: it is presumed that reflections are difficult to be learned and thus, a solution without them is presented to the PINN as first guidance. Training then proceeds over $\num{10000}$ iterations ($\num{7000}$ Adam, $3000$ L-BFGS).

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_gaussian_dirichlet/predictions_calc19.png}
\caption{Model predictions for a PINN trained on Dirichlet boundary conditions with where $\xi'_b = \pm 0.5$ was chosen such that $|A(\xi'_b,\zeta)|^2 \gg 0$. The second normalization is employed with $\xi' = x / 10w_0$ and $\zeta' = z  / 20z_0$.}
\label{fig:gaussianbackreflection}
\end{figure}

It is easily seen that for these settings, one encounters significant reflections in Fig.~\ref{fig:gaussianbackreflection}. Obviously, no analytical solution can be provided for this solution and thus, an indepth discussion of these findings will not be carried out.


\subsection{Oddities: no boundary condition required} \label{subsec:nobound}

Studying the literature, one obtains the impression that PINNs are highly volatile with respect to the boundary conditions. For many problems, this may be the case. The paraxial Helmholtz equations with a Gaussian as initial condition exhibits, peculiarily, the opposite property: the PINN yields excellent results when provided with no boundary condition at all.\par 
As proof, consider Fig.~\ref{fig:gaussianoddity} and compare the results to periodic boundary conditions presented in Fig.~\ref{fig:gaussianperiodicbig}. Here, a PINN was trained without boundary conditions with single precision as well as $\num{12000}$ Adam and $\num{5000}$ L-BFGS iterations. Notably, $m_f = \num{15000}$ was chosen for the number of collocation points (compared to $m_f = \num{10000}$) since this decreases the error significantly (see Fig.~\ref{fig:gaussiannobound_nfcomp}). \par 
The relative $\L_2$ errors for $u$ and $v$ obtained without boundary condition are approximately the same as with periodic boundaries. Interestingly, the $\L_2$ error for $|A|^2$ does not benefit from error cancellation (as seen in Fig.~\ref{fig:gaussianperiodicbig} for periodic boundaries), indicating that the errors for $u$ and $v$ are less systematic. Indeed, untypically, the highest region of error is encountered near the initial condition. \par 
Indeed, this phenomenon does not only occur for $\xi' \in [-1,1]$, where $A(\xi' = \pm 1, \zeta')$ is rather small -- it is also observed for $\xi' \in [-0.5,0.5]$, where for the same PINN with Dirichlet boundary conditions, boundary reflections were observed in Fig.~\ref{fig:gaussianbackreflection}, thus proving that the boundary condition has a significant impact on the solution.

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_gaussian_nobound/predictions_calc28}
\caption{Model predictions for a PINN trained without boundary conditions with the second normalization. Despite the absence of boundary conditions, convergence to the exact analytical solution \eqref{eq:gaussianwaves_2D} (for boundary condition $A(x,z) \rar 0, \ x\rar\infty$) was achieved. It is with respect to this exact solution that the relative $\L_2$ errors are calculated.}
\label{fig:gaussianoddity}
\end{figure}

The paraxial Helmholtz equation without boundary conditions is, technically, an ill-defined problem. Therefore, one would expect a PINN to have to choose between infinitively many solutions, thus either converging to an arbitrary solution or, in fact, no solution whatsoever. Thinking in terms of the loss function, this means that we would expect the PINN to have to choose between countless local minima with wildly different end results. In fact, since reflections could be observed at the boundary for Dirichlet conditions, we definitively know that these kind of local minima exist. Why then does the PINN converge exactly to the solution given by the boundary condition $A(x,z) \rar 0, \ x\rar \infty$? \par 
One intuitive explanation -- albeit seriously lacking in rigour -- could be regarded as an extension of the ideas laid out in the paragraph on \textit{observations on the training process}, earlier this chapter. There, the idea was, crudely put, that the PINN learns the features of the solution from ``easy'' to ``hard''. Extending this argument, one could categorize different boundary conditions as ``easy'' or ``hard'' to learn for the PINN. Perhaps, the boundary condition $A(x,z) \rar 0, \ x\rar\infty$ is, for this PDE, one of the ``easiest''. Obviously, however, this approach has to be formalized to be useful to any degree.\par 
%is that, empirically, PINNs take the ``easiest'' path (see the discussion on the beam width, Fig.~\ref{fig:gaussianperiodicsmall_beamwidth} where this hypothesis was introduced). This, for example, is reflected by the fact that PINNs generally struggle approximating solutions of high frequency. One could imagine that the boundary condition $A(x,z) \rar 0, \ x\rar \infty$ is, for this PDE, one of the ``easiest''. \par
%Still, mathematically, periodic boundary conditions for this problem yield the same result as $A(x,z) \rar 0, \ x\rar \infty$. Why then would limiting the solution space lead to worse performance? %One possible explanation lies in the derivatives which for periodic boundary conditions are calculated at the boundary. Perhaps floating point precision (\texttt{float32} being employed) is a limiting factor thereof.\par 
Overall, further research on this phenomenon will be necessary:
\begin{itemize}
\item Can this behavious be observed for other differential equations?
\item What is the impact of the initial condition? Can a particular solution be selected by different choices of initial conditions?
\item Can a particular solution be selected by providing a small amount of experimental data?
\end{itemize}


\paragraph{Impact of number of collocation points} The number of collocation points $N_f$ provided to the model as points where the partial differential equation is enforced is one of the most important hyperparameters of PINNs. Generally, higher $N_f$ lead to a reduced relative $\L_2$ error (notably however, not a lower cost $J$) at the expense of high computational cost. Comparison of cost and $\L_2$ errors for $m_f = \num{10000}$ and $m_f = \num{15000}$ yields Fig.~\ref{fig:gaussiannobound_nfcomp}. It is evident that, as expected, the cost $J$ for both $N_f$ is approximately equal while the $\L_2$ errors all decrease for higher $m_f$. Presumably, increasing $m_f$ will also lead to lower $\L_2$ errors for previous PINNs.

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_gaussian_nobound/comparison_error_27_28}
\caption{Comparison of cost (blue, right axis) and relative $\L_2$ errors for $u$ (red), $v$ (green) and $|A|^2$ (purple, all left axis) for $m_f = \num{10000}$ (solid) and $m_f = \num{15000}$ (dashed). Notably, the $\L_2$ error decreases for higher $m_f$ while the cost is approximately equal. The sudden decrease in the cost at $\num{12000}$ iterations is fully attributable to the switch to the L-BFGS optimizer at this point in training.}
\label{fig:gaussiannobound_nfcomp}
\end{figure}

On another note, Fig.~\ref{fig:gaussiannobound_nfcomp} also demonstrates that the PINN presented in Chapter~\ref{sec:pinnpoisson} was an anomaly in terms of the relationship between cost and $\L_2$ error. Shown in Fig.~\ref{fig:pinnpoisson_l2_and_cost}, the $\L_2$ error was
\begin{enumerate}
\item highly sensitive to spikes in the cost and
\item increasing after $\num{1600}$ epochs while the cost further decreased.
\end{enumerate}
Neither properties are apparent in Fig.~\ref{fig:gaussiannobound_nfcomp} -- the cost and $\L_2$ error decrease mostly monotonously and the $\L_2$ error is only marginally affected by the spikes in the cost.


\paragraph{Extrapolation capabilities} While the collocation points for the model presented in Fig.~\ref{fig:gaussianoddity} were only placed for $\xi' \in [-1,1]$ and $\zeta' \in [0,1]$, the model itself can provide predictions anywhere. Thus, it is possible to evaluate the extrapolation capabilities of this model by generating predictions in $\xi' \in [-2,2]$ and $\zeta' \in [0,2]$ and comparing against the exact solution. These results are presented in Fig.~\ref{fig:gaussiannobound_extrapol}. 

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_gaussian_nobound_extrapol/int_gauss_compare_all_calc28}
\caption{Comparison of model predictions, exact analytical solution and the error with definitions analogous to Fig.~\ref{fig:gaussianperiodicsmall}. The model used is the same as in Fig.~\ref{fig:gaussianoddity}, however, $\xi' \in[-2,2]$ and $\zeta' \in [0,2]$ was now chosen, thus increasing the area analyzed by a factor of $4$. The region where collocation points were placed (\textit{Training region}) is encased by the dashed, gray line. }
\label{fig:gaussiannobound_extrapol}
\end{figure}

Overall, evaluation of the extrapolation capabilities are highly dependent on the expectation set inbefore: given the expectation that the PINN should try to reasonably and smoothly extend the solution to higher $x$ and $z$ values, the PINN definitely succeeds in this regard. Given the expectation that the PINN should achieve a low $\L_2$ error, it must be concluded that the extrapolation capabilities along the $x$-axis are very limited while extrapolation along the $z$-axis is surprisingly accurate. One simple explanation for this phenomenon is the nature of paraxial waves -- by design, these waves vary only slowly along the $z$-axis. Thus, the $z$-dynamics is much easier to extrapolate. Similar results not shown here were achieved for periodic boundaries.\par 
It can be concluded that for the Gaussian beam propagation, one can use a given PINN to predict the solution at further $z$ values. For higher $x$ values, one may wish to use either sequence-to-sequence learning in order to model the extended region with two PINNs or curriculum learning by initializing the weights with the trained weights of the ``smaller'' PINN, thus significantly reducing training time.

%It is obvious that the extrapolation capabilities of the model are quite limited. Notably, extrapolation along the $z$-axis is much better than along the $x$-axis.  In conclusion, the ability of the PINN to extrapolate is very limited. If the dynamics in the ``untrained region'' is a primitive extension of the ``trained region'', such as for $\zeta' \in [1,2]$, $\xi' \in [-1,1]$, extrapolation may be satisfactory. For other regions (especially $\xi' \in [-1, -2]$ and $\xi' \in [1,2]$), the results are in now way reliable since the high frequencies cannot be extrapolated.\par  
%It has to be concluded that extrapolation for this PINN -- and presumably most PINNs -- is not a viable option. This, however, is neither particulary surprising, nor is it problematic. If one were to decide that the region were a given PINN is trained on is too small, one could either use sequence-to-sequence learning in order to model the extended region with two PINNs or one could set up a new PINN with curriculum learning by initializing the weights with the trained weights of the ``smaller'' PINN. Both of these approaches would lead to a significant time save in the training process.



\section{Summary and discussion}

In this chapter, several PINNs for the Gaussian beam propagation in free space were presented. PINNs for two normalizations were investigated, $\xi = x/w_0$ and $\zeta = z/z_0$ (first normalization) as well as $\xi = x/10w_0$ and $\zeta = z/20z_0$, where the latter beam corresponds to a stronger radius of curvature and thus a more rapidly broadening beam.\par 
First, periodic boundary conditions ($A(x_b, z) = A(-x_b, z)$) were analyzed in Section~\ref{subsec:periodicbound}. For both normalizations, PINNs were successfully employed to predict Gaussian beam propagation (see Fig.~\ref{fig:gaussianperiodicsmall} and~\ref{fig:gaussianperiodicbig}). Most troublesome at approximating were the fringes at the boundary of the beam (see Fig.~\ref{fig:gaussianperiodicbig_slice}). Observations on the training dynamics were presented (see Fig.~\ref{fig:gaussianperiodicbig_progress}) which is, conceptually, mirrored by the training process for the PINNs presented next chapter approximating the parabolic waveguide as well as confirmed by literature. The relative $\L_2$ errors for these approximations vary from $\num{6.7e-3}$ for $|A|^2$ to $\num{2.3e-2}$ for $v$ and will be discussed later this summary.\par 
Subsequently, Dirichlet boundary conditions ($A(\pm x_b, z) = 0$) where investigated. For the first normalization, the solution for Dirichlet boundaries with $\xi = \pm 6$ coincided with the solution given by \eqref{eq:gaussianwaves_2D} (which is the Gaussian beam solution of the paraxial Helmholtz equaation with boundary condition $A(x,b) \rar 0, \ x \rar\infty$) since the latter solution is approximately zero at these boundaries (see Fig.~\ref{fig:gaussiandirichletsmall}). The errors vary from $\num{2.2e-2}$ for $|A|^2$ to $\num{6.1e-2}$ for $v$. For the second normalization, due to the stronger radius of curvature, the beam hits the boundaries. Thus, reflections at the boundaries were observed for Dirichlet boundary conditions (see Fig.~\ref{fig:gaussianbackreflection}). This proves that the PINN can incorporate the boundary condition physically reasonably.\par 
All the more surprising was to observe in Section~\ref{subsec:nobound} that good convergence to \eqref{eq:gaussianwaves_2D} could also be achieved without providing any boundary conditions at all (see Fig.~\ref{fig:gaussianoddity}. For the PINN without boundary condition, it was also studied how well it is able to extrapolate outside the region where collocation points were placed (see Fig.~\ref{fig:gaussiannobound_extrapol}. It was found that in the $z$-direction, extrapolation is possible -- presumably since paraxial waves, by design, do not vary significantly along $z$, the optical axis. However, along the $x$-axis, the extrapolation capabilities of the model are limited. While continuation outside the training region, $|\xi'|>1$, is smooth, the model fails to predict the fringes at the beam boundaries. \par 
The errors found for most models presented, compared with literature results for other 2D problems \cite{raissiPhysicsinformedNeuralNetworks2019}, are not ideal. One easy way to lower the error was shown in Section~\ref{subsec:nobound}, where the number of collocation points was improved, thus reducing the $\L_2$ error by half for $u$ and $v$ and by a factor of four for $|A|^2$. Similarily, a lower error could presumably be achieved with more layers and more nodes per layers. Furthermore, other advanced methods such as learning rate annealing or locally adaptive activation functions could be employed. It is up to the experimentalist to determine how large the error for a particular problem may be in order for the PINN to still be instructive. As always, few ``free lunches'' are encountered in computational physics -- a low error will usually correlate with a higher computational effort. The PINNs presented i`n this Chapter have not been optimized to the fullest; it was rather aimed at striking a good compromise between training speed and relative $\L_2$ error.\par
This marks the end of the discussion of Gaussian beam propagation in free space. The Gaussian beam itself will however return when, among others, a PINN for guiding of a Gaussian beam in a quadratic-index medium is presented next chapter.


