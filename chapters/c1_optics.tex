\chapter{Wave optics} \label{ch:optics}

The physical description of light has been the subject of studies for centuries. Already in the early $19^\mathrm{th}$ century, Fresnel and Fraunhofer successfully employed a scalar theory for light wave propagation. From $1850$ to $1870$, in several groundbreaking works, James C. Maxwell showed how optics as well as electro- and magnetostatics can be unified in the now called \textit{classical electromagnetic theory} \cite{maxwellVIIIDynamicalTheory1865}. He arrived at this fundamental results by amalgamating and expanding upon many laws of electromagnetic theory, finally leading to over 20 coupled equations. Oliver Heaviside, with help of vector notation, managed to reduce these equations to just four -- today known as \textit{Maxwell's equations} \cite{huntOliverHeavisideFirstrate2012}. These coupled partial differential equations, in conjunction with the Lorentz force, describe the whole range of electromagnetism and optics \cite{griffithsIntroductionElectrodynamics2017}.\par 
Fully solving Maxwell's equations for non-standard problems is, generally speaking, a formidable task.  However, the success of wave and beam optics suggests that in many cases, sufficiently accurate scalar representations can be obtained. In this first chapter, a coherent derivation of the \textit{paraxial Helmholtz equation}, whose solutions are, among others, Gaussian beams, will be provided, starting from Maxwell's equations in free space. This particular solution will then, in Chapter~\ref{ch:pinngaussianwave}, be approximated by a physics-informed neural network.\par 
Gaussian beams are highly important in laser physics, where they can describe the fundamental transverse mode TEM$_{00}$ \cite{eichlerLaserBauformenStrahlfuehrung2006}. The more general description of paraxial waves propagating in homogenous media can be achieved via \textit{Hermite-Gaussian beams}, a class of solutions forming a complete basis in which an arbitrary scalar field can be expanded.\par 
Waveguides are optical media where light is guided through a gradient in the refractive index $n(\br)$. This change in refractive index may be abrupt -- at the interface of two different media -- but may also be continuous. Dielectric media with continuously changing refractive index are called \textit{graded-index} media. In such media, given certain assumptions, propagation of paraxial waves can be described analogously to free space with the \textit{paraxial wave equation}. However, for most functions $n(\br)$, an analytic solution cannot be provided. One special case where this is possible is a transverse quadratic dependence without longitudinal variation. The resulting differential equation is structurally identical to the well-known \textit{quantum harmonic oscillator} and its solution can be expressed in closed-form as the previously mentioned Hermite-Gaussian modes. Derivation of these modes will be provided in preparation to Chapter~\ref{ch:pinnwaveguide}, where a PINN for the transverse quadratic-index waveguide will be presented.


\section{From Maxwell's equations to Fresnel diffraction} \label{sec:optics_in_vacuum}

In this section, the transition from vector to scalar optics will be shown, starting from Maxwell's equations in free space. Employing the \textit{Fresnel approximation}, the paraxial Helmholtz equation will be derived, which describes propagation of paraxial waves, i.e., waves which vary slowly with respect to their direction of propagation.


\subsection{Maxwell's equations in free space}

First, consider Maxwell's equations for the \textit{electric field} $\efieldb(\br,t)$ and the \textit{magnetic field} $\bfieldb(\br,t)$ in free space without sources \cite{griffithsIntroductionElectrodynamics2017}:
\begin{align}
\nabla \times \efieldb &= - \ppt \bfieldb \label{eq:induction_vac} \\
\nabla \times \bfieldb &= \mu_0 \eps_0 \ppt \efieldb \label{eq:ampere_vac} \\
\nabla \cdot \efieldb &= 0 \label{eq:gauss_vac} \\
\nabla \cdot \bfieldb &= 0. \label{eq:noname_vac}
\end{align}
We denote the divergence as $\nabla \cdot$ and the curl as $\nabla \times$. Furthermore, $\eps_0$ and $\mu_0$ are the electric permittivity and magnetic permeability of vacuum respectively. 

\subsection{The wave equation}
 
A necessary condition for all components of $\efieldb = (\efield_x, \efield_y, \efield_z)$ and $\bfieldb = (\bfield_x, \bfield_y, \bfield_z)$ to fulfill is the wave equation, which will be derived in the following. For this, apply the curl $\nabla\times$ onto \eqref{eq:induction_vac}
\begin{align}
\nabla \times (\nabla \times \efieldb) &= \nabla \times \left( - \ppt \bfieldb\right) \\
\nabla \times (\nabla \times \efieldb) &= - \ppt \left( \nabla \times \bfieldb\right),
\intertext{substitute with \eqref{eq:ampere_vac}}
\nabla \times (\nabla \times \efieldb) &= - \eps_0 \mu_0 \pptz \efieldb
\end{align}
and apply the vector identity 
\begin{equation} \label{eq:vector_id}
\nabla \times (\nabla \times \efieldb) = \nabla(\nabla \cdot \efieldb) - \nabla^2 \efieldb,
\end{equation}
where $\nabla$ is the gradient and $\nabla^2$ the Laplace operator, in order to obtain
\begin{align}
\nabla(\nabla \cdot \efieldb) - \nabla^2 \efieldb &= -\mu_0 \eps_0 \pptz \efieldb,
\intertext{where $\nabla \cdot \efieldb = 0$ due to \eqref{eq:gauss_vac}, finally yielding}
\nabla^2 \efieldb -\mu_0 \eps_0 \pptz \efieldb &= 0. \label{eq:vectorwaveeq_vac}
\end{align}
With the relation $c_0^2 \: \eps_0 \mu_0 = 1$, where $c_0$ is the speed of light in vacuum, substitution with $1/c_0^2$ is possible. For $\bfieldb$ instead of $\efieldb$, the same formula holds (derived similarily by applying the curl operator onto \eqref{eq:ampere_vac} and substituting analogously). \eqref{eq:vectorwaveeq_vac} is the vectorial wave equation. Consequently, all components $u(\br, t)$ of $\efieldb$ and $\bfieldb$ in vacuum must fulfill the \textit{scalar wave equation}
\begin{equation} \label{eq:scalarwaveeq_vac}
\nabla^2 u(\br, t) - \frac{1}{c_0^2} \pptz u(\br, t) = 0
\end{equation}
as a necessary condition \cite{salehFundamentalsPhotonics2007}.


\subsection{The Helmholtz equation}

The \textit{Helmholtz equation} is a partial differential equation for a monochromatic wave which contains the same information as the wave equation, except in a time-independent form. A monochromatic wave of circular frequency $\omega$ can be constructed as an analytical signal:
\begin{equation} \label{eq:monochromaticu}
u(\br, t) = U(\br) \e^{-\i \omega t}.
\end{equation}
$U(\br)$ is called the complex amplitude.\footnote{Here, the derivation is slightly inprecise since $\efieldb$ and its components are real while $u(\br, t)$ is from now on treated as a complex quantitiy. This transition to the complex numbers offers great mathematical advantages. If at any stage real physical quantities are to be calculated, one only needs to form $\Re\{ u(\br, t) \}$ in order to obtain the components of $\efieldb$.} Substituting \eqref{eq:monochromaticu} into the wave equation \eqref{eq:scalarwaveeq_vac}, we obtain the Helmholtz equation
\begin{equation} \label{eq:helmholtz_monochr_vac}
\nabla^2 U(\br) + k^2 U(\br) = 0,
\end{equation}
where the wave number $k \coloneqq \omega / c_0$ was introduced \cite{salehFundamentalsPhotonics2007}.The Helmholtz equation, containing only spatial derivatives, is fundamentally easier to solve than the wave equation. \par 
Two important solutions to the Helmholtz equation are the \textit{plane wave},
\begin{equation} \label{eq:planewaves}
U(\br) = A_0 \e^{ \i \bk \cdot \br},
\end{equation}
with the \textit{wavevector} $\bk = (k_x, k_y, k_z)$ (where $k_x^2 + k_y^2 + k_z^2 = k^2$) and the complex wave amplitude $A_0$, as well as the \textit{spherical wave},
\begin{equation} \label{eq:sphericalwaves}
U(\br) = \frac{A_0}{r}\e^{\i kr},
\end{equation}
where $k = |\bk |$ and $r = | \br|$.


\subsection{The paraxial Helmholtz equation} \label{subsec:paraxialhelmholtzvac}

The {paraxial Helmholtz equation} is obtained by the {Fresnel approximation}. By demanding that
\begin{equation}
\sqrt{x^2 + y^2} \ll z,
\end{equation}
the solution space is limited to waves which are close to the $z$ axis (i.e. the axis of propagation) but far from the origin \cite{salehFundamentalsPhotonics2007}. Applying this approximation onto the spherical wave, \eqref{eq:sphericalwaves}, we obtain via Taylor-series expansion
\begin{equation}
r = \sqrt{x^2 + y^2 + z^2} = z\sqrt{1 + \frac{x^2 + y^2}{z^2}} \approx z\left(1 + \frac{x^2 + y^2}{2z^2}\right) = z + \frac{x^2 + y^2}{2z},
\end{equation}
yielding the \textit{paraboloidal wave}
\begin{equation} \label{eq:paraboloidalwaves}
U(\br) = \frac{A_0}{z} \e^{\i kz} \exp\left[\i k\frac{x^2 + y^2 }{2 z}\right].
\end{equation}
Here, $A_0/r \approx A_0 / z$ since the magnitude is relatively insensitive to small deviations from the $z$ axis \cite{salehFundamentalsPhotonics2007}.\par 
More generally, we can construct \textit{paraxial waves} with the \textit{ansatz}
\begin{equation} \label{eq:paraxialwaves}
U(\br) = A(\br) \e^{\i k z},
\end{equation}
i.e. that the complex amplitude $A(\br)$ (also called \textit{complex envelope}) is a function with a weak $z$ dependence. Paraxial waves are not a solution of the Helmholtz equation, but by inserting \eqref{eq:paraxialwaves} into the latter, we find
\begin{align}
\ppzz A(\br) + \underbrace{\left(\ppxz + \ppyz\right)}_{\nabla^2_T} A(\br) + 2\i k \ppz A(\br) &= 0,
\end{align}
which, neglecting $\partial^2 A / \partial z^2$ (due to the slowly varying envelope), yields the paraxial Helmholtz equation
\begin{align}
\nabla^2_T A(\br) + 2\i k \ppz A(\br) &= 0 \label{eq:paraxialhelmholtz_vac}
\end{align}
for the complex envelope $A(\br)$, where $\nabla^2_T$ is the transverse Laplacian \cite{salehFundamentalsPhotonics2007}. \par 
The exact solution of \eqref{eq:paraxialhelmholtz_vac} can be expressed via the \textit{Fresnel diffraction integral}. It can easily be verified that the paraboloidal wave satisfies \eqref{eq:paraxialhelmholtz_vac}. More interestingly, the Gaussian wave with complex envelope
\begin{equation} \label{eq:gaussianwaves}
A(\br) = \frac{A_0}{q(z)} \exp\left[\i k\frac{x^2 + y^2}{2q(z)}\right],
\end{equation}
is also a solution of \eqref{eq:paraxialhelmholtz_vac} \cite{salehFundamentalsPhotonics2007}. Here, $q(z) = z - \i z_0$ is called the \textit{q-parameter}, where $z_0$ is the \textit{Rayleigh length}. Separating $1/q(z)$ into its real and imaginary parts, we obtain
\begin{equation}
\frac{1}{q(z)} = \frac{1}{R(z)} + \i \frac{\lambda}{\pi w^2(z)},
\end{equation}
where $R(z)$ and $w(z)$ have been defined as
\begin{align}
R(z) &= z \left[ 1+ \left(\frac{z_0}{z}\right)^2 \right] \\
w(z) &= w_0 \sqrt{1 + \left(\frac{z}{z_0}\right)^2 }. \label{eq:beamwidth}
\end{align}
Visualizing these two equations and comparing them to $A(\br)$, one realizes that $R(z)$ can be identified as a radius of curvature and $w(z)$ as the beam width \cite{salehFundamentalsPhotonics2007}. The beam width at the focus is given by $w_0 = \sqrt{\lambda z_0 / \pi}$. Reinserting into \eqref{eq:gaussianwaves} yields
\begin{equation}
A(\br) = \frac{\i A_0}{z_0} \frac{w_0}{w(z)} \exp\left[ - \frac{x^2 + y^2}{w^2(z)} \right] \exp\left[\i k\frac{x^2 + y^2}{2 R(z)} \right] \exp\left[- \i \psi(z)\right],
\end{equation}
where $\psi (z) \coloneqq \arctan(z / z_0)$ is the \textit{Gouy phase}; a phase shift which the Gaussian beam acquires in its focus \cite{salehFundamentalsPhotonics2007}. {Note that while we have only spoken of propagation in free space, \eqref{eq:gaussianwaves} is also the general equation for propagation of paraxial waves in homogenous media, i.e. media with constant refractive index.} \par 
In Chapter~\ref{ch:pinngaussianwave}, a PINN for solving the paraxial Helmholtz equation will be presented.

%It can be shown that
%\begin{equation}
%z_0 = \frac{\pi n w_0^2}{\lambda},
%\end{equation}
%where $w_0$ is the beam radius at the waist and $\lambda$ is the wavelength \cite{salehFundamentalsPhotonics2007}. The beam radius varies with $z$ according to \cite{salehFundamentalsPhotonics2007}
%\begin{equation}
%w(z) = w_0 \sqrt{1 + \left(\frac{z}{z_0}\right)^2 }.
%\end{equation}
%The Gaussian wave (or \textit{Gaussian beam}) is of paramount importance to optics since it, among others, models the propagation of the most simple laser mode (TEM$_{00}$) \cite{eichlerLaserBauformenStrahlfuehrung2006}. In fact, it is the Gaussian beam which is the first function to be approximated by a physics-informed neural network in Chapter~\ref{ch:opticswithpinns} of this work.



\section{Optical waveguides} \label{sec:optics_in_waveguides}

In principle, a dielectric optical waveguide is a system which transmits an optical beam over long distances by embedding a light channel in a material of higher refractive index. Such waveguides have found extensive use not only in research\footnote{If one wants to confirm this fact themselves, they only need to search for ``waveguide`` on \textit{Google Scholar}, immediately revealing $\num{1740000}$ search results.} but also, among others, in glass fibre cables, enabling the increasing global digitalization \cite{eichlerLaserBauformenStrahlfuehrung2006}.\par 


\subsection{The paraxial Helmholtz equation for a graded-index waveguide} 

%\textbf{TASK: tensor notation or something else for $\chi$}

\par In dielectric media, the electric field $\efield(x,\omega)$ induces a response field, $\pfield(x,\omega)$, called the polarization field, which, while highly complex microscopically, can generally\footnote{Note that this power series is only valid as a phenomenological approach as long as the convergence radius is not met, i.e. the field strengths are small enough \cite{wegenerExtremeNonlinearOptics2010}.} be expressed as a power series of the electric field
\begin{align} \label{eq:polarization}
\pfield &= \eps_0 \left( \chi^{(1)} \efield + \chi^{(2)} \efield^2 + \chi^{(3)} \efield^3 ... \right) ,
\end{align}
where $\chi^{(i)}$ are the $i$-th order electric susceptibilities. For simplicity, this equation was provided for scalar $\efield$ and $\pfield$. For vector fields, the relation has to be written with tensor algebra. Then, $\chi^{(i)}$ is a $(i+1)$-th rank tensor. \par 
Let us consider the three-dimensional propagation of electromagnetic waves in a linear, non-dispersive, isotropic, inhomogenous, non-magnetic and source-free dielectric medium. In a linear medium the polarization response to an electric field is approximately linear, i.e.
\begin{equation}
\pfieldb(\br, \omega) = \eps_0 \chi(\br) \efieldb(\br, \omega).
\end{equation}
Whereas in crystals or other media with an ordered structure, $\chi(\br)$ is a $2^\mathrm{nd}$ rank tensor, it is a scalar in isotropic media. For waveguides, we consider inhomogenous media, hence the spatial dependence of $\chi$. We differentiate between \textit{step-index} and \textit{graded-index} media, depending on whether the susceptibility changes abruptly or continuously \cite{salehFundamentalsPhotonics2007}.\par 
The Maxwell equations are then given by
\begin{align}
\nabla \times \efieldb &= - \ppt \bfield \label{eq:maxinh1} \\
\nabla \times \bfieldb &= \mu_0 \eps(\br) \ppt \efieldb \label{eq:maxinh2} \\
\nabla \cdot (\eps(\br) \, \efieldb) &= 0 \label{eq:maxinh3} \\
\nabla \cdot \bfieldb &= 0,
\end{align}
where the permittivity $\eps(\br) = \eps_0 (1 + \chi(\br))$ has been introduced \cite{salehFundamentalsPhotonics2007}. Note that it is common and often helpful to rewrite these equations using the auxilliary fields $\hfieldb = \bfieldb / \mu_0$ and $\dfieldb = \eps(\br) \, \efieldb$ (here given for linear, non-magnetic media); for obtaining the wave equation however, this is unnecessary. \par 
Since we consider monochromatic waves, we can express the fields as
\begin{align}
\efieldb(\br, t) &= \Re \left\{\E(\br) \e^{-\i \omega t}\right\} \\
\bfieldb(\br, t) &= \Re \left\{\B(\br) \e^{-\i \omega t}\right\}, 
\end{align}
where $\E(\br)$ and $\B(\br)$ are complex-amplitude vectors. 
%\begin{align} \label{eq:maxwell_inhomogenous}
%\nabla \times \E &=  \i \, \omega \B  \\
%\nabla \times \B &= -\i \, \omega \, \mu_0 \eps(\br) \E \\
%\nabla \cdot \E &= 0 \\
%\nabla \cdot \B &= 0 
%\end{align}
The derivation of the wave equation as well as Helmholtz equation is largely analogous to Chapter~\ref{sec:optics_in_vacuum}, with the caveat that $\eps(\br)$ is required to vary sufficiently slowly with respect to the wavelength. The interested reader can follow the individual steps in the appendix. The Helmholtz equation for monochromatic waves in the above defined dielectric medium is then given by
\begin{align} \label{eq:helmholtz_n}
\nabla^2 U(\br) + n^2(\br) k_0^2 \, U(\br) = 0,
\end{align}
where $k_0 = \omega / c_0$ is the wavenumber of propagation in free space and $n(\br) = \sqrt{\eps(\br) / \eps_0}$ the refractive index \cite{salehFundamentalsPhotonics2007}. The complex amplitude $U(\br)$ corresponds to one of the components of $\E$ or $\H$. \par 
The transition to the paraxial wave equation is wholly analogous to Chapter~\ref{subsec:paraxialhelmholtzvac}. We consider paraxial waves
\begin{equation}
U(\br) = A(\br) \e^{\i n_0 k_0 z}
\end{equation}
where $n_0$ is the unperturbed refractive index and $A(\br)$ the slowly varying envelope function. Furthermore, the refractive index is only allowed to vary transversely to the $z$ axis, i.e., $n(\br) \equiv n(x,y)$. Inserting $U(\br)$ into \eqref{eq:helmholtz_n} while neglecting the second-order derivative of $A(\br)$ yields the paraxial wave equation for a varying refractive index:
\begin{align} \label{eq:paraxialwaveeq_n}
\nabla^2_T A(\br) + 2 \i n_0 k_0 \ppz A(\br) + \left( n^2(x,y) - n_0^2 \right) k_0^2 A(\br) = 0.
\end{align}
It is evident that for $n(x,y) = n_0$, \eqref{eq:paraxialwaveeq_n} reduces to the paraxial Helmholtz equation, \eqref{eq:paraxialhelmholtz_vac}.

\subsection{Parabolic waveguide in one dimension} \label{subsec:parabolicwaveguide}

In Chapter~\ref{ch:pinnwaveguide}, a PINN based on the parabolic waveguide in one dimension is presented. In order to assess its merits, a reference solution has to be found. First, we eliminate the $y$-dependence in \eqref{eq:paraxialwaveeq_n} and approximate
\begin{equation}
n^2 (x) - n_0^2 = (n(x) - n_0)(n(x) + n_0) \approx (n(x) - n_0) \cdot 2n_0 \eqqcolon 2n_0 \Delta n(x).
\end{equation}
The refractive index of a parabolic waveguide, as the name suggests, varies quadratically,
\begin{equation}
\Delta n(x) = - ax^2,
\end{equation}
where $a$ is a small proportionality constant so that $n(x) + n_0 \approx 2n_0$ holds true. Thus, the equation to be solved by the PINN is given by
\begin{align} \label{eq:paraxialwaveeq_1d}
\ppxz A(x,z) + 2 \i n_0 k_0 \ppz A(x,z) - 2 a n_0 k_0^2 x^2 A(x,z) = 0.
\end{align}
This partial differential equation mirrors the time-dependent Schrödinger equation for the quantum harmonic oscillator and thus, the same solution techniques can be applied in order to find the exact solution. \par 
With the \textit{ansatz} $A(x,z) \coloneqq G(x) P(z)$, 
\begin{equation}
\underbrace{\frac{1}{G(x)} \left[- \ppxz G(x) + 2an_0k_0^2x^2G(x)\right]}_{\eqqcolon M} = \underbrace{2 \i n_0k_0 \frac{1}{P(z)} \ppz P(z)}_{\eqqcolon M},
\end{equation}
separation of variables is achieved and two independent ordinary differential equations are obtained:
\begin{align}
- \ppxz G(x) + 2an_0k_0^2x^2G(x) &= M G(x) \label{eq:eqforg} \\
\ppz P(z) &= \frac{- \i M}{2n_0k_0}P(z) \label{eq:eqforp} 
\end{align}
The solution for \eqref{eq:eqforp} is simply
\begin{equation}
P(z) = \e^{-\frac{\i M}{2n_0 k_0} z},
\end{equation}
where $P(z = 0) = 1$ was chosen for simplicity. %in order to conserve the norm of the beam.%\footnote{If $|P|\neq 1$ the beam intensity increases by multiplying $G(x)P(z)$, which, devoid of outside sources, is unreasonable, physically speaking.} 
Note that integration over $z$ was performed from $0$ to $z$ (the most general solution would be from $z_0$ to $z$, where $z_0$ is an arbitrary starting value).\par
\eqref{eq:eqforg} is a textbook eigenvalue problem whose solutions are the Hermite-Gausian functions $g_n(x)$ \cite{griffithsIntroductionQuantumMechanics2004},
\begin{align} \label{eq:gaussianhermitesolution}
g_n(x) = \frac{1}{\sqrt{2^n n!}} \left(\frac{k_0}{\pi} \sqrt{2an_0}\right)^{1/4} \e^{-\frac{k_0 x^2}{2} \sqrt{2an_0}} H_n\left((2an_0 k_0^2)^{1/4} x\right),
\end{align}
here normalized by requiring that
\begin{equation} \label{eq:gaussianhermitenormalization}
\inti |g_n(x)|^2 \d x = 1.
\end{equation}
$H_n(z)$ are the Hermite polynomials, of whom several equivalent definitions exist. Computationally convenient and thus provided here is the recursive definition via
\begin{align} \label{eq:hermiterecursion}
H_{n+1} (z) &= 2 z H_n(z) - 2n H_{n-1} (z) \\
H_1(z) &= 2z \\
H_0(z) &= 1.
\end{align}
Inserting \eqref{eq:gaussianhermitesolution} into the differential equation \eqref{eq:paraxialwaveeq_1d} yields the eigenvalues
\begin{equation} \label{eq:eigenvalues}
M_n = k_0 \sqrt{8 an_0} \left(n + \frac{1}{2}\right).
\end{equation}
If we now choose as initial function $A(x,0)$ a stationary state $A(x,0) = g_n(x)$, the full solution of \eqref{eq:paraxialwaveeq_1d} is given by
\begin{equation}
A(x,z) = g_n(x) \e^{-\frac{\i M}{2n_0 k_0} z}.
\end{equation}
Analogously to quantum mechanics, we can expand (almost)\footnote{The requirement is that $G(x)$ is Lebesgue integrable.} any function $G(x)$ into the \textit{eigenbasis}, thus yielding
\begin{equation}
G(x) = \sum_n c_n g_n(x).
\end{equation}
Since any linear combination of solutions of \eqref{eq:paraxialwaveeq_1d} remains a solution of \eqref{eq:paraxialwaveeq_1d}, general solutions $A(x,z)$ can thus be easily constructed via 
\begin{equation} \label{eq:solutionwaveguide}
A(x,z) = \sum_n c_n g_n(x) \e^{-\frac{\i M_n}{2n_0 k_0} z}.
\end{equation}
