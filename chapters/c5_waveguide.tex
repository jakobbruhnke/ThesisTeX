\chapter{Solving the parabolic waveguide} \label{ch:pinnwaveguide}

In order to transmit light, in the ideal case, without loss in a localized region of space, waveguides have been used in optics for more than a century. The parabolic waveguide, being the topic of this chapter is not a realistic waveguide. In a parabolic waveguide, all modes are guided, while a realistic waveguide (accurately approximated as parabolic for narrow beams) is necessarily anharmonic at a given range, due to the impossibility of manufacturing infinitely extended structure and due to physical limitations on the possible values for the refractive index \cite{andreoliMaximumRefractiveIndex2021}. Provided that the beam is sufficiently narrow, the parabolic waveguide can serve as a valuable model in which guided wave propagation can easily be explored since closed-form analytical solutions in the form of Hermite-Gaussian modes exist. A more realistic example for a waveguide would be the case of a Gaussian index profile. Here, modes of the structure include radiation modes, which inherently transport energies along directions differing from the axis of the optical waveguide.\par 
In the following, PINNs for the parabolic waveguide will be presented. Analogously to the Gaussian beam propagation, we will first present the real and imaginary part of the PDE, discuss the boundary and initial condition, normalizations and chosen hyperparameters. Then, three PINNs will be presented, corresponding to
\begin{enumerate}
\item the excitation of the sole fundamental mode,
\item the excitation of the first two modes of the structure and
\item the excitation of four modes, from $n=1$ to $n=4$, of the structure.
\end{enumerate}
This chapter aims to show how a PINN can deal with different initial conditions of increasing complexity for a given PDE. All PINNs presented in this chapter can be found in a \texttt{GitLab} repository, \url{https://gitlab.com/jakobbruhnke/pinnwaveguide}.

\section{Preliminaries -- Setting up the problem}

\paragraph{Real and imaginary components} As derived in Chapter~\ref{subsec:parabolicwaveguide}, the propagation of paraxial waves in a quadratic-index medium is described by an extension of the paraxial wave equation, \eqref{eq:paraxialwaveeq_1d}, which is again provided here for convenience:
\begin{align} \label{eq:paraxialwaveeq_1d_ch4}
\ppxz A(x,z) + 2 \i n_0 k_0 \ppz A(x,z) - 2 a n_0 k_0^2 x^2 \, A(x,z) = 0.
\end{align}
As carried out in Chapter~\ref{ch:pinngaussianwave}, the differential equation will be separated in its real and imaginary part. To this end, let us define
\begin{equation}
A(x,z) \coloneqq u(x,z) + \i v(x,z).
\end{equation}
Substituting in \eqref{eq:paraxialwaveeq_1d_ch4} gives
\begin{multline}
\frac{\partial^2 }{\partial x^2} u(x,z) - 2n_0 k_0 \partial_z v(x,z) - 2an_0 k_0^2 x^2 \, u(x,z) \\ + \i\cdot \left( \frac{\partial^2 }{\partial x^2} v(x,z) + 2n_0 k_0 \partial_z u(x,z) - 2an_0 k_0^2 x^2 \, v(x,z) \right) = 0,
\end{multline}
yielding
\begin{align}
 \frac{\partial^2 }{\partial x^2} u(x,z) - 2n_0 k_0 \partial_z v(x,z) - 2an_0 k_0^2 x^2 \, u(x,z) &= 0, \\
 \frac{\partial^2 }{\partial x^2} v(x,z) + 2n_0 k_0 \partial_z u(x,z) - 2an_0 k_0^2 x^2 \, v(x,z) &= 0,
\end{align}
as the two real-valued partial differential equation constituting the residuals provided to the PINNs.\par 
The stationary solutions of \eqref{eq:paraxialwaveeq_1d_ch4} are the Hermite-Gaussian modes $g_n(x)$, \eqref{eq:gaussianhermitesolution}. The full solution, \eqref{eq:solutionwaveguide}, is thus a superposition of these modes, propagated by a phase factor:
\begin{equation} \label{eq:solutionwaveguide_ch4}
A(x,z) = \sum_n c_n g_n(x) \e^{-\frac{\i M_n}{2n_0 k_0} z}.
\end{equation}
The coefficients $c_n$ corresponding to one particular solution will be conveniently bundled to a coefficient vector $\c = (c_0, ..., c_n, ...)^\mathrm{T}$, fully characterizing a particular solution $A(x,z)$.\par 
Since $A(x,z)$ is complex-valued, the PINN optimizes its real and imaginary part separately. Again, $u(x,z)$ and $v(x,z)$ and thus the initial condition, separated into real and imaginary part, are not stated explicitly here but are rather obtained numerically by utilizing complex datatypes of \texttt{NumPy}.

\paragraph{Boundary condition} Dirichlet boundary conditions
\begin{equation}
A(-x_b, z) = A(x_b, z) = 0,
\end{equation}
with $x = x_b$ the extent of the boundary, will be employed. Since we examine guided modes in a parabolic well in cases where $|A(\pm x_b, z)| \approx 0$, Dirichlet boundary conditions can be expected to yield satisfactory results with respect to the approximation of \eqref{eq:solutionwaveguide_ch4}.

\paragraph{Initial conditions} As stated in Chapter~\ref{subsec:parabolicwaveguide}, the paraxial wave equation for the parabolic waveguide is equivalent to the quantum harmonic oscillator. The term $2 a n_0 k_0^2 x^2$ serves as a potential, induced by the varying refractive index, in which light is confined. The stationary solutions are the Gaussian-Hermite modes, \eqref{eq:gaussianhermitesolution}, with eigenvalues given by \eqref{eq:eigenvalues}. The eigenvalues are equally spaced. An illustration of the first five modes in this potential is provided in Fig.~\ref{fig:waveguide_pot}. 

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_waveguide/waveguide_pot.pdf}
\caption{First five stationary solutions $g_n(x)$ of \eqref{eq:paraxialwaveeq_1d_ch4} in the presence of a parabolic index profile, $\Delta n \propto x^2$.}
\label{fig:waveguide_pot}
\end{figure}

For the following PINNs, three different linear combinations of Hermite-Gaussian modes will be used as initial condition, characterized by the coefficient vectors $\c^{(1)}$, $\c^{(2)}$ and $\c^{(3)}$:

\begin{equation} \label{eq:coefficients}
\c^{(1)} = \begin{pmatrix}
1 \\ 0 \\ \vdots
\end{pmatrix}, \ \ \ \c^{(2)} = \begin{pmatrix}
1 + \i \\ 1 + \i \\ 0 \\ \vdots
\end{pmatrix}, \ \ \ \c^{(3)} = \begin{pmatrix}
0 \\ 1 - 0.75\i \\ 0.75 + 0.5\i \\ 0.5 - 0.25\i \\ 0.25 + 0.1\i \\ 0 \\ \vdots
\end{pmatrix}.
\end{equation}

These coefficients are chosen for the purpose of increasing complexity:  where $\c^{(1)}$ is sole the fundamental mode $n=0$ corresponding to a Gaussian profile, $\c^{(2)}$ is a linear combination of the first two modes ($n=0$ \& $n=1$) and $\c^{(3)}$ is a linear combination of four modes, $n=1$ to $n=4$.\par 
The stationary solutions $g_n(x)$ are normalized so that the integral of their square is one (see \eqref{eq:gaussianhermitenormalization}). Thus, the coefficient vectors $\c$ given in \eqref{eq:coefficients} have to be normalized by

\begin{equation} \label{eq:normalizationcoefficients}
\c_\mathrm{norm} = \frac{\c}{\| \c \|_2} = \frac{\c}{\sqrt{c_0^2 + c_1^2 + ...}}.
\end{equation}

The initial conditions are shown in Fig.~\ref{fig:waveguide_init} for $u(x, z=0)$ and $v(x, z=0)$, where the $x$-axis is normalized by $2\lambda$ (see the next paragraph on normalization).

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_waveguide/init_solutions.pdf}
\caption{Initial solutions for three different coefficient vectors $\c^{(1)}$, $\c^{(2)}$ and $\c^{(3)}$, as given by \eqref{eq:coefficients} and normalized as in \eqref{eq:normalizationcoefficients}. The $x$-axis is normalized by $2\lambda$.}
\label{fig:waveguide_init}
\end{figure}

\paragraph{Normalization} It is most convenient to normalize the equation with respect to the wavelength. With $\xi = \frac{x}{2\lambda}$, $\zeta = \frac{z}{5\lambda}$ and $\beta = a \lambda^2$, the PDE transforms to
\begin{equation} \label{eq:waveguideeq_norm}
\ppxiz A(\xi,\zeta) +  \frac{16}{5} \pi \i n_0 \ppze A(\xi,\zeta) - 64 \pi^2 \beta n_0  \xi^2 \, A(\xi,\zeta) = 0.
\end{equation}
The stationary solutions are now given by
\begin{equation}
g_n(\xi) =  \frac{1}{\sqrt{2^n n!}} \left( 8 \sqrt{\beta n_0} \right)^{1/4} \e^{-4 \pi \xi^2 \sqrt{\beta n_0}} H_n\left(\sqrt{8\pi} (\beta n_0)^{1/4} \xi \right)
\end{equation}
with eigenvalues
\begin{equation}
M_n = 16\pi \sqrt{\beta n_0} \left(n+\frac{1}{2}\right) 
\end{equation}
and thus the full time-dependent solution
\begin{align} \label{eq:waveguidesol_norm}
A(\xi, \zeta) = \sum_n c_n g_n(\xi) \e^{-5 \i \sqrt{\beta / n_0} \left(n + \frac{1}{2}\right) \zeta}.
\end{align}
This normalization was chosen so that the beam is guided within $\xi \in [-1, 1]$ for $\c^{(1)/(2)}$ and $\xi \in [-1.5, 1.5]$ for $\c^{(3)}$; moreover, interesting features of the solution can be observed for $\zeta \in [0,1]$. Throughout this chapter, $\beta = 1$ is chosen as the parameter of the waveguide.

\newpage % formatting 

\paragraph{Hyperparameters and other settings} In Tab.~\ref{tab:hyperparameter_waveguide}, the hyperparameters which remain constant throughout this section are provided, together with other settings.

\begin{table}[H]
    \centering
    \caption{Listing of the hyperparameters and chosen settings which all PINNs presented in this Chapter have in common.}
    \label{tab:hyperparameter_waveguide}
    \rowcolors{1}{tableblue}{White}
    \begin{tabularx}{.95\textwidth}{lcX}
    \arrayrulecolor{darktableblue} \hline 
    Hyperparameter & Symbol & Used for upcoming calculations \\ \hline
    Number of layers & $L$ & $\num{6}$ ($5$ hidden, $1$ output) \\
    Nodes per layer & $n_l$ & $\num{32}$ for all hidden layers \\
    Adam learning rate & $\alpha$ & $\num{1e-3}$  \\
    Collocation points (PDE) & $m_f$ & $\c^{(1) / (2)}$: $\num{10000}$, \newline $\c^{(3)}$: $\num{15000}$ \\
    Collocation points (Bound.) & $m_b$ & $\num{200}$  \\
    Collocation points (Init.) & $m_\mathrm{init}$ & $\num{200}$  \\
    Sampling of colloc. points & -- & Random numbers (uniform distribution) \\
    Weight factor (PDE) & $w_f$ & $\c^{(1) / (2)}$: $\num{5e-4}$, \newline $\c^{(3)}$: $\num{1e-4}$  \\
    Weight factor (boundary) & $w_b$ & $1$ \\
    Weight factor (init.) & $w_\mathrm{init}$ & $\num{1}$  \\
    Precision & -- & $\c^{(1) / (2)}$: single (\texttt{float32}) \newline $\c^{(3)}$: double (\texttt{float64}) \\
    Optimizer & -- & In succession: Adam and L-BFGS  \\
    Activation function & $\mathrm{act}$ & $\tanh$ \\
    Loss function & -- & Mean squared error ($\mathrm{MSE}$) \\ 
    Initialization & -- & Glorot initialization \\
    Training region & -- & $\c^{(1)/(2)}$: $\xi \in [-1, 1]$ and $\zeta \in [0,1]$, \newline $\c^{(3)}$: $\xi \in [-1.5, 1.5]$ and $\zeta \in [0,1]$ \\ \hline
    \end{tabularx}
\end{table}



\section{Training results}

\subsection{Guiding a beam with Gaussian profile} \label{subsec:waveguidegaussian}

As a suitable transition from the previous chapter on Gaussian beam propagation to this chapter, we choose as initial condition a Gaussian profile with planar phase front, corresponding to the coefficient vector $\c^{(1)}$ in \eqref{eq:coefficients}. \par 
Training was completed after $\num{12000}$ Adam and $\num{3000}$ L-BFGS iterations, taking approximately $\SI{40}{\min}$. Single-point precision (\texttt{float32}) proved sufficient for this problem. The results are depicted in Fig.~\ref{fig:waveguide_gaussian}.

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_waveguide/waveguide_compare_all_calc8}
\caption{Comparison of the model predictions (index $\mathrm{pred}$, \textit{left column}) for a Gaussian initial distribution, $\c^{(1)}$, with the exact analytical solution (index $\mathrm{ex}$, \textit{middle column}) and the errors $\Delta \beta = \left| \beta_\mathrm{pred} - \beta_\mathrm{ex}\right|$, $\beta \in \{u, v, |A|^2\}$ (\textit{right column}). The first row shows the results for $u(x,z)$, the second row for $v(x,z)$ and the third row for the intensity $|A(x,z)|^2$, where $A(x,z) = u(x,z) + \i v(x,z)$ is the complex amplitude \eqref{eq:paraxialwaveeq_1d_ch4}. The $x$-axis is normalized by $2\lambda$ and the $z$-axis by $5\lambda$.}
\label{fig:waveguide_gaussian}
\end{figure}

The left column are the model predictions, the center column the exact solution, \eqref{eq:waveguidesol_norm}, and the right column the prediction error. The performance of the PINN is very satisfactory, both qualitatively (visually) and quantitatively in terms of the relative $\L_2$ errors, which are all in the low \textit{per milles}. The PINN is not particularily optimized with respect to the $\L_2$ error since the parameters were also chosen with the computational cost in mind. Consequently, the error could presumably reduced to values around $10^{-4}$, thus being on the lower end of errors typically reported in the literature \cite{raissiPhysicsinformedNeuralNetworks2019, wangUnderstandingMitigatingGradient2021, moseleySolvingWaveEquation2020}.\par 
The errors for $u$ and $v$ appear somewhat periodically in patches, thus suggesting a systematic nature. However, the error for the intensity $|A|^2$ does not exhibit the same behaviour: Its pattern is more complex due to the sensitivity from the phase of the overall field. Note that the error in panel~{i)} is asymmetric with respect to the mirror axis at $x = 0$. Apparently, the PINN is not able to fully take the symmetry axis at $x = 0$ into account.\footnote{Actually, without much effort, it should be possible to add another term to the loss function penalizing solutions which do not fulfill this symmetry. It might be interesting to see if this enforces the expected symmetry in the error.} The nature of the relative $\L_2$ error will be further discussed in Section~\ref{subsec:waveguidecomplicated}.


\subsection[Guiding a superposition of two modes, $n=0$ and $n=1$]{Guiding a superposition of two modes, $\vb*{n=0}$ and $\vb*{n=1}$} \label{subsec:waveguidesimple}

The good performance of the PINN for the guided Gaussian beam can be partly traced back to the simplicity of the problem: no multiscale features, no high-frequency components in the solution\footnote{Again, high frequency in this context does not pertain to optical frequency but rather to the spatial spectrum of the propagating beam.}, a simple initial condition, all of them have a positive impact on convergence.\par 
In this section, a PINN for a linear combination of the first two Hermite-Gaussian modes as characterized by $\c^{(2)}$, \eqref{eq:normalizationcoefficients}, will be presented. Training was performed for $\num{12000}$ Adam and $\num{4000}$ L-BFGS iterations with
single point precision, taking approximately $\SI{50}{\min}$. The results are shown in Fig.~\ref{fig:waveguide_simplesuperpos}.

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_waveguide/waveguide_compare_all_calc11}
\caption{Comparison of the model predictions for an initial distribution determined by the coefficients $\c^{(2)}$, \eqref{eq:coefficients}, with the exact analytical solution. The column and row assignment as well as error definition and normalization is identical to Fig.~\ref{fig:waveguide_gaussian}.}
\label{fig:waveguide_simplesuperpos}
\end{figure}

Similar to the Gaussian beam, good convergence was achieved. Perhaps surprisingly, the $\L_2$ errors for $u$, $v$ and $|A|^2$ are of the same order of magnitude, at $10^{-3}$, as for the Gaussian beam propagation, despite higher spatial frequency components being present in the solution. Noticeably, the error grows with $\zeta$. This is congruent to statements made in Chapter~\ref{sec:pinnpoisson}, where it was noted that PINNs learn best in regions where explicit data is provided.


\paragraph{The training process} In Chapter~\ref{subsec:dirichletbound}, Fig.~\ref{fig:gaussiandirichletsmall_progress}, the training process was investigated visually through different snapshots of the solution at an increasing number of iterations. The same analysis has been conducted for the previously presented PINN whose solution was characterized by the coefficient vector $\c^{(2)}$. In Fig.~\ref{fig:waveguide_simplesuperpos_progress}, this analysis is presented for four stages during training ($3000$, $8000$, $\num{12000}$ and $\num{16000}$ iterations) and compared to the exact solution.

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_waveguide/progress2_calc11.png}
\caption{Training process of the beam intensity $|A(x,z)|^2$ for the PINN presented in Fig.~\ref{fig:waveguide_simplesuperpos} at four different iterations ($3000$, $8000$, $\num{12000}$ and $\num{16000}$) compared to the exact solution (panel {e)}).}
\label{fig:waveguide_simplesuperpos_progress}
\end{figure}

Firstly, one may wonder why the first panel {a)} starts at already $\num{3000}$ iterations (compared to Fig.~\ref{fig:gaussiandirichletsmall_progress}{a)} starting at $500$ iterations). What, in fact, happens during the first $\num{1000}$ iterations? The answer is quite simply and perhaps surprisingly: \textit{almost nothing} -- at least visually. For the parabolic waveguide with the given normalization, \eqref{eq:waveguideeq_norm} and independently of the chosen initial condition, after ca. $\num{100}$ iterations we find ourselves at a plateau region on the loss surface at high loss values. Actual features of the solution only start emerging after approximately $\num{1000}$ iterations. This is a rather undesirable property: for the PINNs presented in this chapter, it takes up to $\num{5000}$ iterations before a confident guess on the ``success chances'' of the currently training PINN can be made. If the location and extend of these plateaus is predictable, a possible remedy could be to drastically increase the Adam learning rate $\alpha$ while in this plateau, e.g. by employing a learning rate scheduler as implemented in \texttt{TensorFlow}. \par 
Studying the training process through all iterations, again, three distinct stages can be observed:\newpage
\begin{enumerate}
\item Prediction of a beam absorbed in propagation%\footnote{Note that this beam is indeed vanishing and not diffracting -- the integral over $|A|^2$ vanishes for $\zeta \rightarrow 0$, whereas for a diffracting beam, $|A|^2$ should be constant for all $\zeta$, provided the beam does not reach the boundaries (which, due to the enforcement of Dirichlet boundary conditions, is true).}
, where the initial condition is fulfilled first.
\item Correct prediction of the general shape of the solution.
\item Incorporation of sharper features; improvement of the amplitude prediction.
\end{enumerate}
These three stages correspond well to the ones observed for Gaussian beam propagation. Generalized to an arbitrary partial differential equation, the following three stages are proposed to be universally applicable to the training process of PINNs:
\begin{enumerate}
\item Fulfillment of initial and boundary conditions where the PDE is primitively satisfied near the boundaries and unsatisfied elsewhere.
\item Adoption of the most dominant features of the solution (``low-frequency components'').
\item Training of high-frequency components.
\end{enumerate}
These observations do not constitute novel research but rather confirm findings in existing publications. Especially, the observation that for FFNNs low-frequency components are trained first is also known in literature as the \textit{Frequency Principle} \cite{xuFrequencyPrincipleFourier2020}. Markidis has previously reported analogous behavior for the training dynamics of PINNs \cite{markidisOldNewCan2021}. A similar dynamics is observed with standard relaxation schemes such as the Gauss-Seidel, where this problem is tackled by using the multigrid integration scheme.

%If these stages are truly universal, there could be a conceivable use of this \textbf{erkenntnis} for curriculum learning. Curriculum learning is applicable when, for a new problem, there already exists a trained PINN which has a similar solution. In this case, during the training process, the high-freq


% But what characterizes this similarity? In this author's opinion, similarity between two solutions 


%Thcould potentially be used to efficiently taylor the employment of curriculum learning. 


%Although curriculum learning has not been used for any of the PINNs presented in this thesis, this author has nonetheless employed it for obtaining other results not shown here. Based on this experience, a hypothesis for smart utilization of curriculum learning will be proposed, which, to the author's best knowledge, has not been communicated this clearly in the literature \textbf{CHECK}: \textit{For most cases, curriculum learning should yield equivalent results regardless if the initial weights are supplied by a model trained with Adam only or a model trained with both Adam and L-BFGS.} The basis for this hypothesis is as follows: L-BFGS is significantly more capable in the training of high-frequency components than Adam. However, curriculum learning in most cases will be employed for problems for which a similar but still significantly different solution has been successfully approximated by a PINN. Thus, most probably, the aspects in which these solutions differ are the high-frequency rather than low-frequency components. The training of such high-frequency components for a solution which is to be used as initialization is thus, for most cases, a wasted effort since the model to be trained using curriculum learning will in all likelihood overwrite these high-frequency regions immediately since they would in this case constitute regions of high cost. In other words, the hypothesis is based on the assumption that the similarity of two solutions is generally governed by the similarity of low-frequency rather than high-frequency components.

\subsection[Guiding a superposition of four modes, $n=1$ to $n=4$]{Guiding a superposition of four modes, $\vb*{n=1}$ to $\vb*{n=4}$} \label{subsec:waveguidecomplicated}

Finally, the PINN is put to the test by employing the initial conditions characterized by $\c^{(3)}$. The step-up in difficulty of training is rather high: as a matter of fact, it was required to train for $\num{20000}$ Adam and $\num{30000}$ L-BFGS iterations with $m_f = \num{15000}$ number of collocation points and double precision (\texttt{float64}) in order to achieve satisfactory results. Additionally, lowering the weight $w_f$ of the PDE residual to $\num{1e-4}$ improves convergence. Training took approximately $\SI{20}{\hour}$ on an Intel Core i7-6600U. The results obtained are presented in Fig.~\ref{fig:waveguide_complsuperpos}.\par 
While the prediction and exact solution show great visual correspondence, the relative $\L_2$ errors are all approximately $\num{1e-2}$ and thus rather high; more than three times the error reported in Fig.~\ref{fig:waveguide_gaussian} and~\ref{fig:waveguide_simplesuperpos}. In the following the error of the PINN shall be discussed further.  %The error is concentrated at later $\zeta$ values, congruent with earlier observations that PINNs train best in regions where explicit data is provided. 

\paragraph{On the error of PINNs} Visually, there is almost no difference between the predicted and the exact solution discernable from Fig.~\ref{fig:waveguide_complsuperpos}. Let us thus consider the region of highest error of the intensity $|A|^2$ in panel {i)} with $\zeta = \frac{z}{5\lambda} = 0.64$ (marked there with a vertical gray dashed line) and $\xi = \frac{x}{2\lambda} = -0.35$. Plotting $u$, $v$ and $|A|^2$ at this $\zeta$-value yields Fig.~\ref{fig:waveguide_complsuperpos_slice}.


\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_waveguide/waveguide_compare_all_calc12}
\caption{Comparison of the model predictions for an initial distribution determined by the coefficient vector $\c^{(3)}$, \eqref{eq:coefficients}, with the exact analytical solution. The column and row assignment as well as error definition and normalization are identical to Fig.~\ref{fig:waveguide_gaussian}. At $\zeta = \frac{z}{5\lambda} = 0.64$, the vertical gray dashed line in the right column indicates where a slice will be further analyzed in Fig.~\ref{fig:waveguide_complsuperpos_slice}.}
\label{fig:waveguide_complsuperpos}
\end{figure}

Fig.~\ref{fig:waveguide_complsuperpos_slice} demonstrates that the highest error of the intensity $|A|^2$ corresponds to a very slight underestimation of the intensity at $\xi = \frac{x}{2\lambda} = -0.35$. Compared to last chapter, where a similar snapshot at a certain $z$ value was discussed for the Gaussian beam propagation in Fig.~\ref{fig:gaussiandirichletsmall_slice}, the deviation seems insignificant and one may wonder why in both cases relative $\L_2$ errors of similar magnitude were found. This, however, is easily explained: for Gaussian beam propagation, effectively all the error is concentrated in the beam fringes while the central region of the beam is approximated very well. For the PINN modelling the parabolic waveguide, the error is distributed more equally (although, as will be discussed shortly, it still exhibits periodic structure). Eventually, it adds up to the same value when integrated all over the integration domain. \par 
One may argue that the latter -- an equally distributed error -- is preferrable to an error strongly localized in one region (in my opinion, it is). An even distribution of error makes a method more robust in the sense that it is easier to evaluate the accuracy of the model predictions in cases were no reference solution is available. It is clear from this thesis that the distribution of error is highly dependent on the PDE. In general, throughout this thesis, it was found that performance of PINNs depends critically both on the PDE and on the initial and boundary conditions. %One may of course not expect a method to work equally well for all initial and boundary conditions.

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_waveguide/mid_solution_calc12}
\caption{Solution of $u$, $v$ and $|A|^2$ at $\frac{z}{5\lambda} = 0.64$. This $z$ value was chosen since, as seen in Fig.~\ref{fig:waveguide_complsuperpos}, it corresponds to the highest error in the intensity.}
\label{fig:waveguide_complsuperpos_slice}
\end{figure}

Moving on in our discussion about the error, note that the errors for $u$ and $v$ for all the three initial conditions exhibit periodic structure. This behaviour is most pronounced for $\c^{(3)}$ (Fig.~\ref{fig:waveguide_complsuperpos}) and least for $\c^{(1)}$ (Fig.~\ref{fig:waveguide_gaussian}). The spatial frequency of these errors, both on the $x$ and $z$ axis, increases from $\c^{(1)}$ to $\c^{(3)}$. This correlates with the increasing highest-order mode present in the solution ($n=0$ for $\c^{(1)}$, $n=1$ for $\c^{(2)}$ and $n=4$ for $\c^{(3)}$). This corresponds very well to the Frequency-principle which states that the highest frequency components are approximated last in neural network training \cite{xuFrequencyPrincipleFourier2020}. To provide numbers for the periodic structure along the $x$-axis: for $\c^{(1)}$, we observe three rows of regions of high error, for $\c^{(2)}$ four rows and for $\c^{(3)}$ six rows; as seen in Fig.~\ref{fig:waveguide_gaussian} ({c, f}), Fig.~\ref{fig:waveguide_simplesuperpos} ({c, f}) and Fig.~\ref{fig:waveguide_complsuperpos} ({c, f}) respectively. While these number of rows definitely correlate with the highest mode in the solution, it is not an exact correspondence. As so often with PINNs, there remains a certain element of unpredictability which is inherent to machine learning methods. %Here, again, we find that training PINNs is not an exact science. A neural network is, effectively, a \textit{blackbox}: it is simply not achievable to completely understand the dynamics behind training and convergence and thus, an element of surprise will always be maintained.

\begin{comment}
: for $\c^{(3)}$, the error has a spatial frequency of $6$ along the $x$-axis. However, it is unfortunately not as simple as, e.g., the spatial frequency along the $x$-axis corresponding to the number of \textbf{wendepunkte} of the highest mode of the initial solution. The reason is that the error has in fact no mirror symmetry at $x=0$. As proof, consider \textbf{FIGURE}, where for non-positive $x$-values, the errors in one row along $z$ are marked with a horizontal, dashed, gray line. Mirroring these lines at the mirror plane $x=0$ (horizontal, dotted gray line), it is clear that the mirrored, dotted lines do not overlay with the error. This behaviour is not easily explainable. If the hypothesis is in fact that the spatial frequency of the highest mode governs the structure of the error, then we could reasonably expect the error to adhere to the symmetry dicated by this mode. Given that this expectation is, apparently, unfounded, it just further goes to show that the output produced by PINNs is less predictable than desirable.

\begin{figure}[H]
\centering
\includegraphics{graphics/chapter4_waveguide/errorsymmetry_calc12}
\caption{\textit{Left:} error $|u_\mathrm{pred}(x,z) - u_\mathrm{ex}(x,z)|$, as in Fig.~\ref{fig:waveguide_complsuperpos}{c)}. Gray dashed lines are manually placed to highlight the location of the three lower rows of high error regions. These lines are mirrored to the positive axis and shown as dotted gray. It is clear that the mirrored lines do not align with the three upper rows of high error regions, thus showing that the error does not adhere to a mirror symmetry. \textit{Right:} the fourth mode of the Hermite-Gaussian stationary solutions $g_n(x)$, as given by \eqref{eq:gaussianhermitesolution}.}
\label{fig:waveguide_complsuperpos_slice}
\end{figure}
\end{comment}


%However, no formula for the spatial frequency can be reasonably provided: firstly, it does't equal the amount of \textbf{wendepunkte} ($#wendepunkte(n) = n+2$), since this only works for $\c^{(3)}$ with $#wendepunkte = 6$ but not for $\c^{(2)}$ where 







\section{Summary and discussion}

This chapter, three initial conditions, defined by coefficient vectors $\c$, \eqref{eq:coefficients}, were provided to a PINN solving the parabolic waveguide:
\begin{enumerate}
\item a Gaussian initial distribution, $\c^{(1)}$
\item a linear combination of the first two Hermite-Gaussian modes, $\c^{(2)}$
\item a linear combination of four Hermite-Gaussian modes, $n=1$ to $n=4$, $\c^{(3)}$
\end{enumerate}
The main question investigated was how PINNs for this problem can manage different initial conditions. First, in Section~\ref{subsec:waveguidegaussian}, the Gaussian initial distribution was chosen. Relative $\L_2$ errors in the lower \textit{per milles} range were achieved, which is standard for PINNs. \par 
A linear superposition of the first two Hermite-Gaussian modes was chosen as initial condition in Section~\ref{subsec:waveguidesimple}. Similar to Chapter~\ref{subsec:dirichletbound} (Fig.~\ref{fig:gaussiandirichletsmall_progress}) the training process was visually (Fig.~\ref{fig:waveguide_simplesuperpos_progress}) dissected into three stages. These three stages were generalized to describe the training process of PINNs for arbitrary PDEs. Agreement to the Frequency-principle \cite{xuFrequencyPrincipleFourier2020} was observed. Fig.~\ref{fig:waveguide_simplesuperpos_progress} also illustrates one finding shared throughout the literature \cite{markidisOldNewCan2021}: L-BFGS is absolutely vital as an optimization method. For all PINNs tested in this thesis, nothing boosted performance as much as employing this second-order method as a subsequent optimization routine.\par 
Finally, with the third initial condition, we came, close to the limit of what is achievable for this PINN. With $\c^{(3)}$ as a coefficient vector, the errors increased more than threefold compared to $\c^{(1)}$ and $\c^{(2)}$ while training time increased more than twentyfold. It is at this point that methods such as sequence-to-sequence learning can be especially valuable.\footnote{However, since the solutions change very drastically for different initial conditions, it is doubtful (although prossible) that curriculum learning is helpful for these problems.} Also, given that the weight $w_f$ was adjusted manually by \textit{trial and error} for this PINN, we have to mention the learning rate annealing algorithm \cite{wangUnderstandingMitigatingGradient2021} which automatically determines optimal weights at each training epoch. 


\begin{comment}
\begin{itemize}
\item this chapter: three initial conditions for the parabolic waveguide where chosen:
\begin{itemize}
\item gaussian initial distribution $c^{(1)}$
\item linear comb. of first two Hermite-Gaussian modes $c^{(2)}$
\item linear comb. of four Hermite-Gaussian modes, $n=1$ to $n=4$, $c^{(3)}$
\end{itemize}
\item question: how the PINNs can manage different conditions?
\item first, section xxx, the gaussian initial distribution $c^{(1)}$ was chosen. Decently low errors in the per milles were achieved
\item with similar errors, a simple superposition of the first two Hermite-Gaussian modes was chosen as initial condition in Section XXX. similar to chapter~\ref{subsec:dirichletbound}, the training process was dissected. The same three phases during the training process found for Gaussian beam propagation in free space can be seen in Fig.~\ref{fig:waveguide_simplesuperpos_progress}. This figure also illustrates one finding mirrored throughout the literature: L-BFGS is absolutely vital as optimization -- for all PINNs tested in this thesis, nothing boosted performance as much switching to this second-order method as a subsequent optimization routine.
\item finally, with the third boundary condition, we came, perhaps, close to the limit of what is achievable for this PINN. With $c^{(3)}$ as a set of coefficients, the errors increased thirtyfold compared to $c^{(1)}$ and $c^{(2)}$.
\end{itemize}
\end{comment}










